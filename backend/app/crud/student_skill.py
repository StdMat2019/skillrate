from uuid import UUID
from typing import List

from sqlalchemy import select
from sqlalchemy import desc
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import StudentSkill
from ..database.models import Student
from ..database.models import Skill


class StudentSkillCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_student_skill(self, student_skill_data: dict) -> StudentSkill:
        required_fields = ["student_id", "skill_id"]
        for field in required_fields:
            if field not in student_skill_data or not student_skill_data[field]:
                raise ValueError(f"{field} is required.")

        new_student_skill = StudentSkill(**student_skill_data)
        self.db.add(new_student_skill)
        await self.db.commit()
        await self.db.refresh(new_student_skill)
        return new_student_skill

    async def create_student_skill_class(self, new_student_skill: StudentSkill) -> StudentSkill:
        # Проверка наличия ключевых полей
        required_fields = ["student_id", "skill_id"]
        for field in required_fields:
            if not getattr(new_student_skill, field, None):
                raise ValueError(f"{field} is required.")

        # Получение связанных объектов Student и Skill
        student = await self.db.execute(select(Student).filter(Student.student_id == new_student_skill.student_id))
        skill = await self.db.execute(select(Skill).filter(Skill.skill_id == new_student_skill.skill_id))

        # Проверка наличия связанных объектов
        if not student.scalar() or not skill.scalar():
            raise ValueError("Invalid student_id or skill_id. Student or Skill not found.")

        # Добавление и сохранение новой StudentSkill
        self.db.add(new_student_skill)
        await self.db.commit()
        await self.db.refresh(new_student_skill)
        return new_student_skill

    async def get_student_skill_by_ids(self, student_id: UUID, skill_id: UUID) -> StudentSkill:
        result = await self.db.execute(
            select(StudentSkill)
            .filter(StudentSkill.student_id == student_id)
            .filter(StudentSkill.skill_id == skill_id)
        )
        return result.scalars().first()

    async def get_all_student_skills_by_id(self, student_id: UUID, limit: int = None) -> List[StudentSkill]:
        if not isinstance(student_id, UUID):
            raise ValueError("Invalid student_id type. Expected UUID.")

        query = (select(StudentSkill).filter(StudentSkill.student_id == student_id)
                 .order_by(desc(StudentSkill.successful_points/StudentSkill.total_points)))

        if limit is not None:
            query = query.limit(limit)

        result = await self.db.execute(query)
        return result.scalars().all()

    async def get_all_students_skills(self) -> List[StudentSkill]:
        result = await self.db.execute(select(StudentSkill))
        return result.scalars().all()

    async def update_student_skill(self, student_id: UUID, skill_id: UUID, update_data: dict) -> StudentSkill:
        result = await self.db.execute(
            select(StudentSkill)
            .filter(StudentSkill.student_id == student_id)
            .filter(StudentSkill.skill_id == skill_id)
        )
        student_skill = result.scalars().first()
        if student_skill:
            for key, value in update_data.items():
                setattr(student_skill, key, value)
            await self.db.commit()
            await self.db.refresh(student_skill)
        return student_skill

    async def delete_student_skill(self, student_id: UUID, skill_id: UUID) -> None:
        result = await self.db.execute(
            select(StudentSkill)
            .filter(StudentSkill.student_id == student_id)
            .filter(StudentSkill.skill_id == skill_id)
        )
        student_skill = result.scalars().first()
        if student_skill:
            await self.db.delete(student_skill)
            await self.db.commit()
