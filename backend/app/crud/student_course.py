from uuid import UUID
from typing import List

import loguru
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import StudentCourse
from ..database.models import Student
from ..database.models import Course


class StudentCourseCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_student_course(self, student_course_data: dict) -> StudentCourse:
        required_fields = ["course_id", "student_id"]
        for field in required_fields:
            if field not in student_course_data or not student_course_data[field]:
                raise ValueError(f"{field} is required.")

        new_student_course = StudentCourse(**student_course_data)
        self.db.add(new_student_course)
        await self.db.commit()
        await self.db.refresh(new_student_course)
        return new_student_course

    async def create_student_course_class(self, student_course: StudentCourse) -> StudentCourse:
        # Проверка наличия ключевых полей
        required_fields = ["course_id", "student_id"]
        for field in required_fields:
            if not getattr(student_course, field, None):
                raise ValueError(f"{field} is required.")

        course = await self.db.execute(select(Course).filter(Course.course_id == student_course.course_id))
        student = await self.db.execute(select(Student).filter(Student.student_id == student_course.student_id))

        if not course.scalar() or not student.scalar():
            raise ValueError("Invalid course_id or student_id. Course or Student not found.")

        # Добавление и сохранение новой StudentCourse
        self.db.add(student_course)
        await self.db.commit()
        await self.db.refresh(student_course)
        return student_course

    async def get_student_course_by_ids(self, course_id: UUID, student_id: UUID) -> StudentCourse:
        result = await self.db.execute(
            select(StudentCourse)
            .filter(StudentCourse.course_id == course_id)
            .filter(StudentCourse.student_id == student_id)
        )
        return result.scalars().first()

    async def get_all_by_student_id(self, student_id: UUID) -> StudentCourse:
        result = await self.db.execute(
            select(StudentCourse)
            .filter(StudentCourse.student_id == student_id)
        )
        return result.scalars().all()

    async def get_all_by_students_id_group_id(self, student_ids: list, course_id: UUID) -> StudentCourse:
        result = await self.db.execute(
            select(StudentCourse)
            .filter(StudentCourse.course_id == course_id)
            .filter(StudentCourse.student_id.in_(student_ids))
        )
        loguru.logger.info(student_ids)
        loguru.logger.info(course_id)
        return result.scalars().all()

    async def get_all_student_courses(self) -> List[StudentCourse]:
        result = await self.db.execute(select(StudentCourse))
        return result.scalars().all()

    async def update_student_course(self, course_id: UUID, student_id: UUID, update_data: dict) -> StudentCourse:
        result = await self.db.execute(
            select(StudentCourse)
            .filter(StudentCourse.course_id == course_id)
            .filter(StudentCourse.student_id == student_id)
        )
        student_course = result.scalars().first()
        if student_course:
            for key, value in update_data.items():
                setattr(student_course, key, value)
            await self.db.commit()
            await self.db.refresh(student_course)
        return student_course

    async def delete_student_course(self, course_id: UUID, student_id: UUID) -> None:
        result = await self.db.execute(
            select(StudentCourse)
            .filter(StudentCourse.course_id == course_id)
            .filter(StudentCourse.student_id == student_id)
        )
        student_course = result.scalars().first()
        if student_course:
            await self.db.delete(student_course)
            await self.db.commit()
