from typing import List
from uuid import UUID

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from .student_course import StudentCourseCRUD
from .student_crud import StudentCRUD
from ..database.models import Course
from ..database.models import CourseGroup
from ..database.models import Group
from ..database.models import StudentCourse


class CourseGroupCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_course_group(self, course_group_data: dict) -> CourseGroup:
        new_course_group = CourseGroup(**course_group_data)
        self.db.add(new_course_group)
        await self.db.commit()
        await self.db.refresh(new_course_group)
        crud_student = StudentCRUD(self.db)
        crud_student_course = StudentCourseCRUD(self.db)
        students = await crud_student.get_students_by_group_id(course_group_data["group_id"])
        for student in students:
            student_course = StudentCourse(course_id=course_group_data["group_id"], student_id=student.student_id,
                                           result=0, complete_percent=0, avg_score=0)
            await crud_student_course.create_student_course_class(student_course)
        return new_course_group

    async def create_course_group_class(self, course_group: CourseGroup) -> CourseGroup:
        # Проверка наличия ключевых полей
        required_fields = ["group_id", "course_id"]
        for field in required_fields:
            if not getattr(course_group, field, None):
                raise ValueError(f"{field} is required.")

        # Получение связанных объектов Group и Course
        group = await self.db.execute(select(Group).filter(Group.group_id == course_group.group_id))
        course = await self.db.execute(select(Course).filter(Course.course_id == course_group.course_id))

        # Проверка наличия связанных объектов
        if not group.scalar() or not course.scalar():
            raise ValueError("Invalid group_id or course_id. Group or Course not found.")

        # Добавление и сохранение новой CourseGroup
        self.db.add(course_group)
        await self.db.commit()
        # После добавления таблицы также дополнительно должны создаваться записи student_course
        await self.db.refresh(course_group)
        crud_student = StudentCRUD(self.db)
        crud_student_course = StudentCourseCRUD(self.db)
        students = await crud_student.get_students_by_group_id(course_group.group_id)
        for student in students:
            student_course = StudentCourse(course_id=course_group.course_id, student_id=student.student_id,
                                           result=0, complete_percent=0, avg_score=0)
            await crud_student_course.create_student_course_class(student_course)

        return course_group

    async def get_course_group(self, group_id: UUID, course_id: UUID) -> CourseGroup:
        result = await self.db.execute(
            select(CourseGroup).filter_by(group_id=group_id, course_id=course_id)
        )
        return result.scalars().first()

    async def get_course_by_group_id(self, group_id: UUID) -> List[Course]:
        query = select(Course).join(CourseGroup).filter_by(group_id=group_id)
        result = await self.db.execute(query)
        return result.scalars().all()

    async def get_course_by_course_id(self, course_id: UUID) -> List:

        query = (
            select(Course, CourseGroup, Group)
            .join(CourseGroup, Course.course_id == CourseGroup.course_id)
            .join(Group, CourseGroup.group_id == Group.group_id)
            .where(Course.course_id == str(course_id))
        )

        result = await self.db.execute(query)
        data = result.fetchall()
        return [dict(zip(result.keys(), row)) for row in data]

    async def get_all_course_groups(self) -> List[CourseGroup]:
        result = await self.db.execute(select(CourseGroup))
        return result.scalars().all()

    async def update_course_group(self, group_id: UUID, course_id: UUID, update_data: dict) -> CourseGroup:
        result = await self.db.execute(
            select(CourseGroup).filter_by(group_id=group_id, course_id=course_id)
        )
        course_group = result.scalars().first()
        if course_group:
            for key, value in update_data.items():
                setattr(course_group, key, value)
            await self.db.commit()
            await self.db.refresh(course_group)
        return course_group

    async def delete_course_group(self, group_id: UUID, course_id: UUID) -> None:
        result = await self.db.execute(
            select(CourseGroup).filter_by(group_id=group_id, course_id=course_id)
        )
        course_group = result.scalars().first()
        if course_group:
            await self.db.delete(course_group)
            await self.db.commit()
