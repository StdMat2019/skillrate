from typing import List
from uuid import UUID

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import QuestStat


class QuestStatCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_quest_stat(self, quest_stat_data: dict) -> QuestStat:
        required_fields = ["test_id", "question_id", "total_points", "successful_points"]
        for field in required_fields:
            if field not in quest_stat_data or not quest_stat_data[field]:
                raise ValueError(f"{field} is required.")

        new_quest_stat = QuestStat(**quest_stat_data)
        self.db.add(new_quest_stat)
        await self.db.commit()
        await self.db.refresh(new_quest_stat)
        return new_quest_stat

    async def create_quest_stat_class(self, question_stat: QuestStat) -> QuestStat:
        required_fields = ["test_id", "student_id", "question_id", "total_points"]
        for field in required_fields:
            if not getattr(question_stat, field, None):
                raise ValueError(f"{field} is required.")

        self.db.add(question_stat)
        await self.db.commit()
        await self.db.refresh(question_stat)
        return question_stat

    async def get_quest_stat_by_test_id(self, test_id: UUID) -> List[QuestStat]:
        if not isinstance(test_id, UUID):
            raise ValueError("Invalid test_id type. Expected UUID.")

        result = await self.db.execute(select(QuestStat).filter(QuestStat.test_id == test_id))
        return result.scalars().all()

    async def get_quest_stat_by_student_question(self, student_id: UUID, question_id: UUID) -> QuestStat:
        if not isinstance(student_id, UUID):
            raise ValueError("Invalid student_id type. Expected UUID.")
        if not isinstance(question_id, UUID):
            raise ValueError("Invalid question_id type. Expected UUID.")

        result = await self.db.execute(select(QuestStat)
                                       .filter(QuestStat.question_id == question_id)
                                       .filter(QuestStat.student_id == student_id))
        return result.scalars().first()

    async def get_quest_stat_by_student(self, student_id: UUID) -> List[QuestStat]:
        if not isinstance(student_id, UUID):
            raise ValueError("Invalid student_id type. Expected UUID.")

        result = await self.db.execute(select(QuestStat)
                                       .filter(QuestStat.student_id == student_id))
        return result.scalars().all()

    async def update_quest_stat(self, test_id: UUID, question_id: UUID, update_data: dict) -> QuestStat:
        if not all(isinstance(id, UUID) for id in [test_id, question_id]):
            raise ValueError("Invalid test_id or question_id type. Expected UUID.")

        result = await self.db.execute(
            select(QuestStat).filter(QuestStat.test_id == test_id, QuestStat.question_id == question_id)
        )
        quest_stat = result.scalars().first()
        if quest_stat:
            for key, value in update_data.items():
                setattr(quest_stat, key, value)
            await self.db.commit()
            await self.db.refresh(quest_stat)
        return quest_stat

    async def delete_quest_stat(self, test_id: UUID, question_id: UUID) -> None:
        if not all(isinstance(id, UUID) for id in [test_id, question_id]):
            raise ValueError("Invalid test_id or question_id type. Expected UUID.")

        result = await self.db.execute(
            select(QuestStat).filter(QuestStat.test_id == test_id, QuestStat.question_id == question_id)
        )
        quest_stat = result.scalars().first()
        if quest_stat:
            await self.db.delete(quest_stat)
            await self.db.commit()
            return quest_stat

    async def get_quest_stat_by_question_id(self, question_id: UUID) -> List[QuestStat]:
        if not isinstance(question_id, UUID):
            raise ValueError("Invalid question_id type. Expected UUID.")

        result = await self.db.execute(select(QuestStat).filter(QuestStat.question_id == question_id))
        return result.scalars().all()
