from uuid import UUID
from typing import List

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import CourseSpeciality


class CourseSpecialityCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_course_speciality(self, course_speciality_data: dict) -> CourseSpeciality:
        required_fields = ["course_id", "speciality_id"]
        for field in required_fields:
            if field not in course_speciality_data or not course_speciality_data[field]:
                raise ValueError(f"{field} is required.")

        new_course_speciality = CourseSpeciality(**course_speciality_data)
        self.db.add(new_course_speciality)
        await self.db.commit()
        await self.db.refresh(new_course_speciality)
        return new_course_speciality

    async def create_course_speciality_class(self, new_course_speciality) -> CourseSpeciality:

        self.db.add(new_course_speciality)
        await self.db.commit()
        await self.db.refresh(new_course_speciality)

        return new_course_speciality

    async def get_course_speciality_by_ids(self, course_id: UUID, speciality_id: UUID) -> CourseSpeciality:
        result = await self.db.execute(
            select(CourseSpeciality)
            .filter(CourseSpeciality.course_id == course_id)
            .filter(CourseSpeciality.speciality_id == speciality_id)
        )
        return result.scalars().first()

    async def get_all_course_specialities(self) -> List[CourseSpeciality]:
        result = await self.db.execute(select(CourseSpeciality))
        return result.scalars().all()

    async def update_course_speciality(self, course_id: UUID, speciality_id: UUID, update_data: dict) -> CourseSpeciality:
        result = await self.db.execute(
            select(CourseSpeciality)
            .filter(CourseSpeciality.course_id == course_id)
            .filter(CourseSpeciality.speciality_id == speciality_id)
        )
        course_speciality = result.scalars().first()
        if course_speciality:
            for key, value in update_data.items():
                setattr(course_speciality, key, value)
            await self.db.commit()
            await self.db.refresh(course_speciality)
        return course_speciality

    async def delete_course_speciality(self, course_id: UUID, speciality_id: UUID) -> None:
        result = await self.db.execute(
            select(CourseSpeciality)
            .filter(CourseSpeciality.course_id == course_id)
            .filter(CourseSpeciality.speciality_id == speciality_id)
        )
        course_speciality = result.scalars().first()
        if course_speciality:
            await self.db.delete(course_speciality)
            await self.db.commit()
