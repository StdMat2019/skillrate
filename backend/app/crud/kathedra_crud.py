from typing import List
from uuid import UUID

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import Kathedra


class KathedraCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_kathedra(self, kathedra_data: dict) -> Kathedra:
        required_fields = ["name"]
        for field in required_fields:
            if field not in kathedra_data or not kathedra_data[field]:
                raise ValueError(f"{field} is required.")

        if len(kathedra_data["name"]) > 20:
            raise ValueError("Name is too long.")

        new_kathedra = Kathedra(**kathedra_data)
        self.db.add(new_kathedra)
        await self.db.commit()
        await self.db.refresh(new_kathedra)
        return new_kathedra

    async def create_kathedra_class(self, kathedra: Kathedra) -> Kathedra:
        # Проверка наличия ключевых полей
        required_fields = ["name"]
        for field in required_fields:
            if not getattr(kathedra, field, None):
                raise ValueError(f"{field} is required.")

        if len(kathedra.name) > 20:
            raise ValueError("Name is too long.")

        self.db.add(kathedra)
        await self.db.commit()
        await self.db.refresh(kathedra)
        return kathedra

    async def get_kathedra_by_id(self, kathedra_id: UUID) -> Kathedra:
        if not isinstance(kathedra_id, UUID):
            raise ValueError("Invalid kathedra_id type. Expected UUID.")

        result = await self.db.execute(select(Kathedra).filter(Kathedra.kathedra_id == kathedra_id))
        return result.scalars().first()

    async def get_all_kathedral(self) -> List[Kathedra]:
        result = await self.db.execute(select(Kathedra))
        return result.scalars().all()

    async def update_kathedra(self, kathedra_id: UUID, update_data: dict) -> Kathedra:
        if not isinstance(kathedra_id, UUID):
            raise ValueError("Invalid kathedra_id type. Expected UUID.")

        result = await self.db.execute(select(Kathedra).filter(Kathedra.kathedra_id == kathedra_id))
        kathedra = result.scalars().first()
        if kathedra:
            for key, value in update_data.items():
                setattr(kathedra, key, value)
            await self.db.commit()
            await self.db.refresh(kathedra)
        return kathedra

    async def delete_kathedra(self, kathedra_id: UUID) -> None:
        if not isinstance(kathedra_id, UUID):
            raise ValueError("Invalid kathedra_id type. Expected UUID.")

        result = await self.db.execute(select(Kathedra).filter(Kathedra.kathedra_id == kathedra_id))
        kathedra = result.scalars().first()
        if kathedra:
            await self.db.delete(kathedra)
            await self.db.commit()
            return kathedra.kathedra_id
        return None

    async def get_kathedra_id_by_name(self, kathedra_name: str) -> UUID:
        if not kathedra_name:
            raise ValueError("Kathedra name is required.")

        result = await self.db.execute(select(Kathedra).filter(Kathedra.name == kathedra_name))
        kathedra = result.scalars().first()
        if kathedra:
            return kathedra
        return None