from typing import List
from uuid import UUID

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import Skill


class SkillCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_skill(self, skill_data: dict) -> Skill:
        required_fields = ["name"]
        for field in required_fields:
            if field not in skill_data or not skill_data[field]:
                raise ValueError(f"{field} is required.")

        if len(skill_data["name"]) > 20:
            raise ValueError("Name is too long.")

        new_skill = Skill(**skill_data)
        self.db.add(new_skill)
        await self.db.commit()
        await self.db.refresh(new_skill)
        return new_skill

    async def create_skill_class(self, skill: Skill) -> Skill:
        # Проверка наличия ключевых полей
        required_fields = ["name"]
        for field in required_fields:
            if not getattr(skill, field, None):
                raise ValueError(f"{field} is required.")

        if len(skill.name) > 20:
            raise ValueError("Name is too long.")

        self.db.add(skill)
        await self.db.commit()
        await self.db.refresh(skill)
        return skill

    async def get_skill_by_id(self, skill_id: UUID) -> Skill:
        if not isinstance(skill_id, UUID):
            raise ValueError("Invalid skill_id type. Expected UUID.")

        result = await self.db.execute(select(Skill).filter(Skill.skill_id == skill_id))
        return result.scalars().first()

    async def get_skill_by_name(self, skill_name: str) -> Skill:
        result = await self.db.execute(select(Skill).filter(Skill.name == skill_name))
        return result.scalars().first()

    async def get_all_skills(self) -> List[Skill]:
        result = await self.db.execute(select(Skill))
        return result.scalars().all()

    async def update_skill(self, skill_id: UUID, update_data: dict) -> Skill:
        if not isinstance(skill_id, UUID):
            raise ValueError("Invalid skill_id type. Expected UUID.")

        result = await self.db.execute(select(Skill).filter(Skill.skill_id == skill_id))
        skill = result.scalars().first()
        if skill:
            for key, value in update_data.items():
                setattr(skill, key, value)
            await self.db.commit()
            await self.db.refresh(skill)
        return skill

    async def delete_skill(self, skill_id: UUID) -> None:
        if not isinstance(skill_id, UUID):
            raise ValueError("Invalid skill_id type. Expected UUID.")

        result = await self.db.execute(select(Skill).filter(Skill.skill_id == skill_id))
        skill = result.scalars().first()
        if skill:
            await self.db.delete(skill)
            await self.db.commit()
            return skill.skill_id
        return None
