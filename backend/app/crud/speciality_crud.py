from uuid import UUID
from typing import List

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import Speciality


class SpecialityCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_speciality(self, speciality_data: dict) -> Speciality:
        # Проверка наличия ключевых полей
        required_fields = ["number", "name"]
        for field in required_fields:
            if field not in speciality_data or not speciality_data[field]:
                raise ValueError(f"{field} is required.")

        if len(speciality_data["name"]) > 20:
            raise ValueError("Name is too long.")

        new_speciality = Speciality(**speciality_data)
        self.db.add(new_speciality)
        await self.db.commit()
        await self.db.refresh(new_speciality)
        return new_speciality

    async def create_speciality_class(self, speciality: Speciality) -> Speciality:
        # Проверка наличия ключевых полей
        required_fields = ["name"]
        for field in required_fields:
            if not getattr(speciality, field, None):
                raise ValueError(f"{field} is required.")

        if len(speciality.name) > 50:
            raise ValueError("Name is too long.")

        self.db.add(speciality)
        await self.db.commit()
        await self.db.refresh(speciality)
        return speciality

    async def get_speciality_by_id(self, speciality_id: UUID) -> Speciality:
        if not isinstance(speciality_id, UUID):
            raise ValueError("Invalid speciality_id type. Expected UUID.")

        result = await self.db.execute(select(Speciality).filter(Speciality.speciality_id == speciality_id))
        return result.scalars().first()

    async def get_all_specialities(self) -> List[Speciality]:
        result = await self.db.execute(select(Speciality))
        return result.scalars().all()

    async def get_speciality_id_by_number(self, number: str) -> UUID:
        result = await self.db.execute(select(Speciality).filter(Speciality.number == number))
        speciality = result.scalars().first()
        if speciality:
            return speciality.speciality_id
        return None

    async def get_speciality_id_by_name(self, name: str) -> UUID:
        result = await self.db.execute(select(Speciality).filter(Speciality.name == name))
        speciality = result.scalars().first()
        if speciality:
            return speciality.speciality_id
        return None

    async def update_speciality(self, speciality_id: UUID, update_data: dict) -> Speciality:
        if not isinstance(speciality_id, UUID):
            raise ValueError("Invalid speciality_id type. Expected UUID.")

        result = await self.db.execute(select(Speciality).filter(Speciality.speciality_id == speciality_id))
        speciality = result.scalars().first()
        if speciality:
            for key, value in update_data.items():
                setattr(speciality, key, value)
            await self.db.commit()
            await self.db.refresh(speciality)
        return speciality

    async def delete_speciality(self, speciality_id: UUID) -> None:
        if not isinstance(speciality_id, UUID):
            raise ValueError("Invalid speciality_id type. Expected UUID.")

        result = await self.db.execute(select(Speciality).filter(Speciality.speciality_id == speciality_id))
        speciality = result.scalars().first()
        if speciality:
            await self.db.delete(speciality)
            await self.db.commit()
