from uuid import UUID
from typing import List

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import TestGroupCourse
from ..database.models import Teacher
from ..database.models import Group
from ..database.models import Course


class TestGroupCourseCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_test_group_course(self, test_group_course_data: dict) -> TestGroupCourse:
        required_fields = ["course_id", "collection_name", "is_open"]
        for field in required_fields:
            if field not in test_group_course_data or not test_group_course_data[field]:
                raise ValueError(f"{field} is required.")

        new_test_group_course = TestGroupCourse(**test_group_course_data)
        self.db.add(new_test_group_course)
        await self.db.commit()
        await self.db.refresh(new_test_group_course)
        return new_test_group_course

    async def create_test_group_course_class(self, test_group_course: TestGroupCourse) -> TestGroupCourse:
        # Проверка наличия ключевых полей
        required_fields = ["course_id", "collection_name"]
        for field in required_fields:
            if not getattr(test_group_course, field, None):
                raise ValueError(f"{field} is required.")

        # Получение связанных объектов Group и Course
        course = await self.db.execute(select(Course).filter(Course.course_id == test_group_course.course_id))
        # Проверка наличия связанных объектов
        if not course.scalar():
            raise ValueError("Invalid course_id. Course not found.")

        # Добавление и сохранение новой TestGroupCourse
        self.db.add(test_group_course)
        await self.db.commit()
        await self.db.refresh(test_group_course)
        return test_group_course

    async def get_test_group_course_by_id(self, course_id: UUID) -> TestGroupCourse:
        result = await self.db.execute(
            select(TestGroupCourse)
            .filter(TestGroupCourse.course_id == course_id)
        )
        return result.scalars().all()

    async def get_test_group_by_course_collection_id(self, course_id: UUID, collection_name: str) -> TestGroupCourse:
        result = await self.db.execute(
            select(TestGroupCourse)
            .filter(TestGroupCourse.course_id == course_id)
            .filter(TestGroupCourse.collection_name == collection_name)
        )
        return result.scalars().first()

    async def get_all_test_group_courses(self) -> List[TestGroupCourse]:
        result = await self.db.execute(select(TestGroupCourse))
        return result.scalars().all()

    async def update_test_group_course(self, course_id: UUID, update_data: dict) -> TestGroupCourse:
        result = await self.db.execute(
            select(TestGroupCourse)
            .filter(TestGroupCourse.course_id == course_id)
        )
        test_group_course = result.scalars().first()
        if test_group_course:
            for key, value in update_data.items():
                setattr(test_group_course, key, value)
            await self.db.commit()
            await self.db.refresh(test_group_course)
        return test_group_course

    async def delete_test_group_course(self, course_id: UUID) -> None:
        result = await self.db.execute(
            select(TestGroupCourse)
            .filter(TestGroupCourse.course_id == course_id)
        )
        test_group_course = result.scalars().first()
        if test_group_course:
            await self.db.delete(test_group_course)
            await self.db.commit()
