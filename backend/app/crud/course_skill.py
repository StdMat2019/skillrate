from uuid import UUID
from typing import List

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import CourseSkill
from ..database.models import Skill
from ..database.models import Course


class CourseSkillCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_course_skill(self, course_skill_data: dict) -> CourseSkill:
        required_fields = ["course_id", "skill_id"]
        for field in required_fields:
            if field not in course_skill_data or not course_skill_data[field]:
                raise ValueError(f"{field} is required.")

        new_course_skill = CourseSkill(**course_skill_data)
        self.db.add(new_course_skill)
        await self.db.commit()
        await self.db.refresh(new_course_skill)
        return new_course_skill

    async def create_course_skill_class(self, course_skill: CourseSkill) -> CourseSkill:
        # Проверка наличия ключевых полей
        required_fields = ["course_id", "skill_id"]
        for field in required_fields:
            if not getattr(course_skill, field, None):
                raise ValueError(f"{field} is required.")

        # Получение связанных объектов Course и Skill
        course = await self.db.execute(select(Course).filter(Course.course_id == course_skill.course_id))
        skill = await self.db.execute(select(Skill).filter(Skill.skill_id == course_skill.skill_id))

        # Проверка наличия связанных объектов
        if not course.scalar() or not skill.scalar():
            raise ValueError("Invalid course_id or skill_id. Course or Skill not found.")

        # Добавление и сохранение новой CourseSkill
        self.db.add(course_skill)
        await self.db.commit()
        await self.db.refresh(course_skill)

        return course_skill
    async def get_course_skill_by_ids(self, course_id: UUID, skill_id: UUID) -> CourseSkill:
        result = await self.db.execute(
            select(CourseSkill)
            .filter(CourseSkill.course_id == course_id)
            .filter(CourseSkill.skill_id == skill_id)
        )
        return result.scalars().first()

    async def get_all_course_skills(self) -> List[CourseSkill]:
        result = await self.db.execute(select(CourseSkill))
        return result.scalars().all()

    async def update_course_skill(self, course_id: UUID, skill_id: UUID, update_data: dict) -> CourseSkill:
        result = await self.db.execute(
            select(CourseSkill)
            .filter(CourseSkill.course_id == course_id)
            .filter(CourseSkill.skill_id == skill_id)
        )
        course_skill = result.scalars().first()
        if course_skill:
            for key, value in update_data.items():
                setattr(course_skill, key, value)
            await self.db.commit()
            await self.db.refresh(course_skill)
        return course_skill

    async def delete_course_skill(self, course_id: UUID, skill_id: UUID) -> None:
        result = await self.db.execute(
            select(CourseSkill)
            .filter(CourseSkill.course_id == course_id)
            .filter(CourseSkill.skill_id == skill_id)
        )
        course_skill = result.scalars().first()
        if course_skill:
            await self.db.delete(course_skill)
            await self.db.commit()
