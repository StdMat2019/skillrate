from typing import List
from uuid import UUID

from sqlalchemy import func
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import Course


class CourseCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_course(self, course_data: dict) -> Course:
        # Validate required fields
        required_fields = ["name", "version", "avg_score", "color", "fullname"]
        for field in required_fields:
            if field not in course_data:
                raise ValueError(f"{field} is required.")

        if len(course_data["name"]) > 20:
            raise ValueError("Name is too long.")

        new_course = Course(**course_data)
        self.db.add(new_course)
        await self.db.commit()
        await self.db.refresh(new_course)
        return new_course

    async def create_course_class(self, course: Course) -> Course:
        required_fields = ["name", "color", "fullname"]
        for field in required_fields:
            if not getattr(course, field, None):
                raise ValueError(f"{field} is required.")

        if len(course.name) > 20:
            raise ValueError("Name is too long.")

        self.db.add(course)
        await self.db.commit()
        await self.db.refresh(course)
        return course

    async def get_course_by_id(self, course_id: UUID) -> Course:
        if not isinstance(course_id, UUID):
            raise ValueError("Invalid course_id type. Expected UUID.")

        result = await self.db.execute(select(Course).filter(Course.course_id == course_id))
        return result.scalars().first()

    async def get_course_by_name(self, course_name: str) -> Course:
        result = await self.db.execute(select(Course).filter(func.lower(Course.name) == course_name.lower()))
        return result.scalars().first()

    async def get_all_courses(self) -> List[Course]:
        result = await self.db.execute(select(Course))
        return result.scalars().all()

    async def update_course(self, course_id: UUID, update_data: dict) -> Course:
        if not isinstance(course_id, UUID):
            raise ValueError("Invalid course_id type. Expected UUID.")

        result = await self.db.execute(select(Course).filter(Course.course_id == course_id))
        course = result.scalars().first()
        if course:
            for key, value in update_data.items():
                setattr(course, key, value)
            await self.db.commit()
            await self.db.refresh(course)
        return course

    async def delete_course(self, course_id: UUID) -> None:
        if not isinstance(course_id, UUID):
            raise ValueError("Invalid course_id type. Expected UUID.")

        result = await self.db.execute(select(Course).filter(Course.course_id == course_id))
        course = result.scalars().first()
        if course:
            await self.db.delete(course)
            await self.db.commit()
            return course.course_id
        else:
            return None
