from typing import List
from uuid import UUID

from sqlalchemy import func
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import Role


class RoleCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_role(self, role_data: dict) -> Role:
        required_fields = ["name"]
        for field in required_fields:
            if field not in role_data or not role_data[field]:
                raise ValueError(f"{field} is required.")

        if len(role_data["name"]) > 20:
            raise ValueError("Name is too long.")

        new_role = Role(**role_data)
        self.db.add(new_role)
        await self.db.commit()
        await self.db.refresh(new_role)
        return new_role

    async def create_role_class(self, role: Role) -> Role:
        # Проверка наличия ключевых полей
        required_fields = ["name"]
        for field in required_fields:
            if not getattr(role, field, None):
                raise ValueError(f"{field} is required.")

        if len(role.name) > 20:
            raise ValueError("Name is too long.")

        self.db.add(role)
        await self.db.commit()
        await self.db.refresh(role)
        return role

    async def get_role_by_id(self, role_id: UUID) -> Role:
        if not isinstance(role_id, UUID):
            raise ValueError("Invalid role_id type. Expected UUID.")

        result = await self.db.execute(select(Role).filter(Role.role_id == role_id))
        return result.scalars().first()

    async def get_role_by_name(self, name: str) -> Role:
        result = await self.db.execute(select(Role).filter(func.lower(Role.name) == name.lower()))
        return result.scalars().first()

    async def get_all_roles(self) -> List[Role]:
        result = await self.db.execute(select(Role))
        return result.scalars().all()

    async def update_role(self, role_id: UUID, update_data: dict) -> Role:
        if not isinstance(role_id, UUID):
            raise ValueError("Invalid role_id type. Expected UUID.")

        result = await self.db.execute(select(Role).filter(Role.role_id == role_id))
        role = result.scalars().first()
        if role:
            for key, value in update_data.items():
                setattr(role, key, value)
            await self.db.commit()
            await self.db.refresh(role)
        return role

    async def delete_role(self, role_id: UUID) -> None:
        if not isinstance(role_id, UUID):
            raise ValueError("Invalid role_id type. Expected UUID.")

        result = await self.db.execute(select(Role).filter(Role.role_id == role_id))
        role = result.scalars().first()
        if role:
            await self.db.delete(role)
            await self.db.commit()
            return role.role_id
        return None
