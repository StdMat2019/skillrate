from uuid import UUID
from typing import List
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import Group


class GroupCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_group(self, group_data: dict) -> Group:
        # Validate required fields
        required_fields = ["name", "start_date", "end_date", "speciality_id", "count_members"]
        for field in required_fields:
            if field not in group_data:
                raise ValueError(f"{field} is required.")

        if len(group_data["name"]) > 20:
            raise ValueError("Name is too long.")

        new_group = Group(**group_data)
        self.db.add(new_group)
        await self.db.commit()
        await self.db.refresh(new_group)
        return new_group

    async def create_group_class(self, group: Group) -> Group:
        # Validate required fields
        required_fields = ["name", "start_date", "end_date", "speciality_id"]
        for field in required_fields:
            if not getattr(group, field, None):
                raise ValueError(f"{field} is required.")

        if len(group.name) > 30:
            raise ValueError("Name is too long.")

        self.db.add(group)
        await self.db.commit()
        await self.db.refresh(group)
        return group

    async def get_group_by_id(self, group_id: UUID) -> Group:
        if not isinstance(group_id, UUID):
            raise ValueError("Invalid group_id type. Expected UUID.")

        result = await self.db.execute(select(Group).filter(Group.group_id == group_id))
        return result.scalars().first()

    async def get_group_by_name(self, name: str) -> Group:
        result = await self.db.execute(select(Group).filter(Group.name == name))
        return result.scalars().first()

    async def get_all_groups(self) -> List[Group]:
        result = await self.db.execute(select(Group))
        return result.scalars().all()

    async def update_group(self, group_id: UUID, update_data: dict) -> Group:
        if not isinstance(group_id, UUID):
            raise ValueError("Invalid group_id type. Expected UUID.")

        result = await self.db.execute(select(Group).filter(Group.group_id == group_id))
        group = result.scalars().first()
        if group:
            for key, value in update_data.items():
                setattr(group, key, value)
            await self.db.commit()
            await self.db.refresh(group)
        return group

    async def delete_group(self, group_id: UUID) -> None:
        if not isinstance(group_id, UUID):
            raise ValueError("Invalid group_id type. Expected UUID.")

        result = await self.db.execute(select(Group).filter(Group.group_id == group_id))
        group = result.scalars().first()
        if group:
            await self.db.delete(group)
            await self.db.commit()
            return group.group_id
        else:
            return None
