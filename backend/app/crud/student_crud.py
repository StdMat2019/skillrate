from typing import List
from uuid import UUID
from fastapi.security import OAuth2PasswordBearer
from fastapi import Depends, HTTPException, status
from jose import jwt
from jose.exceptions import JWTError
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import Student
from ..auth.hash_passwd import bcrypt

#ВЫНЕСТИ ВЫНЕСТИ ВЫНЕСТИ ВЫНЕСТИ ВЫНЕСТИ
oauth2_scheme = OAuth2PasswordBearer(tokenUrl='token')
SECRET_KEY = '52367badbf4e42f3a94d9ce456e1f01cbfee36a604da5c9589fa84f0bb9e661b'  # вынести в env


class StudentCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_student(self, student_data: dict) -> Student:
        # Проверка наличия ключевых полей
        required_fields = ["name", "login", "hashed_passwd"]
        for field in required_fields:
            if field not in student_data or not student_data[field]:
                raise ValueError(f"{field} is required.")

        if len(student_data["name"]) > 20:
            raise ValueError("Name is too long.")
        student_data["hashed_passwd"] = bcrypt(student_data["hashed_passwd"])
        new_student = Student(**student_data)
        self.db.add(new_student)
        await self.db.commit()
        await self.db.refresh(new_student)
        return new_student

    async def create_student_class(self, student: Student) -> Student:
        # Проверка наличия ключевых полей
        required_fields = ["name", "login", "hashed_passwd"]
        for field in required_fields:
            if not getattr(student, field, None):
                raise ValueError(f"{field} is required.")

        if len(student.name) > 20:
            raise ValueError("Name is too long.")
        student.hashed_passwd = bcrypt(student.hashed_passwd)
        self.db.add(student)
        await self.db.commit()
        await self.db.refresh(student)
        return student

    async def get_student_by_id(self, student_id: UUID) -> Student:
        if not isinstance(student_id, UUID):
            raise ValueError("Invalid student_id type. Expected UUID.")

        result = await self.db.execute(select(Student).filter(Student.student_id == student_id))
        return result.scalars().first()

    async def get_students_by_group_id(self, group_id: UUID) -> List[Student]:
        if not isinstance(group_id, UUID):
            raise ValueError("Invalid group_id type. Expected UUID.")

        result = await self.db.execute(select(Student).filter(Student.group_id == group_id))
        return result.scalars().all()

    async def get_student(self, login: str):
        result = await self.db.execute(select(Student).filter(Student.login == login))
        return result.scalars().first()

    async def get_all_students(self) -> List[Student]:
        result = await self.db.execute(select(Student))
        return result.scalars().all()

    async def update_student(self, student_id: UUID, update_data: dict) -> Student:
        if not isinstance(student_id, UUID):
            raise ValueError("Invalid student_id type. Expected UUID.")

        result = await self.db.execute(select(Student).filter(Student.student_id == student_id))
        student = result.scalars().first()
        if student:
            for key, value in update_data.items():
                setattr(student, key, value)
            await self.db.commit()
            await self.db.refresh(student)
        return student

    async def delete_student(self, student_id: UUID) -> None:
        if not isinstance(student_id, UUID):
            raise ValueError("Invalid student_id type. Expected UUID.")

        result = await self.db.execute(select(Student).filter(Student.student_id == student_id))
        student = result.scalars().first()
        if student:
            await self.db.delete(student)
            await self.db.commit()

    async def get_current_student(self, token: str = Depends(oauth2_scheme)):
        credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
        try:
            if not token:
                return None
            payload = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
            login: str = payload.get("login")
            if login is None:
                raise credentials_exception
        except JWTError:
            raise credentials_exception
        user = await self.get_student(login)
        if user is None:
            raise credentials_exception

        return user
