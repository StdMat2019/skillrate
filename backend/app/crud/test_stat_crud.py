from typing import List
from uuid import UUID
from sqlalchemy import desc
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import TestStat


class TestStatCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_test_stat(self, test_stat_data: dict) -> TestStat:
        # Проверка наличия ключевых полей
        required_fields = ["student_id", "date", "total_time", "total_points", "successful_points"]
        for field in required_fields:
            if field not in test_stat_data or test_stat_data[field] is None:
                raise ValueError(f"{field} is required.")

        new_test_stat = TestStat(**test_stat_data)
        self.db.add(new_test_stat)
        await self.db.commit()
        await self.db.refresh(new_test_stat)
        return new_test_stat

    async def create_test_stat_class(self, test_stat: TestStat) -> TestStat:
        # Проверка наличия ключевых полей
        required_fields = ["student_id", "date", "total_time", "total_points", "successful_points"]
        for field in required_fields:
            if not getattr(test_stat, field, None):
                raise ValueError(f"{field} is required.")

        self.db.add(test_stat)
        await self.db.commit()
        await self.db.refresh(test_stat)
        return test_stat

    async def get_test_stat_by_id(self, test_id: UUID) -> TestStat:
        if not isinstance(test_id, UUID):
            raise ValueError("Invalid test_id type. Expected UUID.")

        result = await self.db.execute(select(TestStat).filter(TestStat.test_id == test_id))
        return result.scalars().first()

    async def get_test_stats_by_stud_id(self, student_id: UUID,
                                        limit: int = None) -> List[TestStat]:
        if not isinstance(student_id, UUID):
            raise ValueError("Invalid student_id type. Expected UUID.")

        query = select(TestStat).filter(TestStat.student_id == student_id).order_by(desc(TestStat.date))

        if limit is not None:
            query = query.limit(limit)

        result = await self.db.execute(query)
        return result.scalars().all()

    async def get_test_stats_by_course_id(self, course_id: UUID) -> List[TestStat]:
        if not isinstance(course_id, UUID):
            raise ValueError("Invalid student_id type. Expected UUID.")

        query = select(TestStat).filter(TestStat.course_id == course_id)

        result = await self.db.execute(query)
        return result.scalars().all()

    async def get_test_stats_by_stud_id_teacher_id(self, student_id: UUID, course_id: UUID,
                                        limit: int = None) -> List[TestStat]:
        if not isinstance(student_id, UUID):
            raise ValueError("Invalid student_id type. Expected UUID.")

        query = (select(TestStat).filter(TestStat.student_id == student_id)
                 .filter(TestStat.course_id == course_id).order_by(desc(TestStat.date)))

        if limit is not None:
            query = query.limit(limit)

        result = await self.db.execute(query)
        return result.scalars().all()

    async def get_all_test_stats(self) -> List[TestStat]:
        result = await self.db.execute(select(TestStat))
        return result.scalars().all()

    async def update_test_stat(self, test_id: UUID, update_data: dict) -> TestStat:
        if not isinstance(test_id, UUID):
            raise ValueError("Invalid test_id type. Expected UUID.")

        result = await self.db.execute(select(TestStat).filter(TestStat.test_id == test_id))
        test_stat = result.scalars().first()
        if test_stat:
            for key, value in update_data.items():
                setattr(test_stat, key, value)
            await self.db.commit()
            await self.db.refresh(test_stat)
        return test_stat

    async def delete_test_stat(self, test_id: UUID) -> None:
        if not isinstance(test_id, UUID):
            raise ValueError("Invalid test_id type. Expected UUID.")

        result = await self.db.execute(select(TestStat).filter(TestStat.test_id == test_id))
        test_stat = result.scalars().first()
        if test_stat:
            await self.db.delete(test_stat)
            await self.db.commit()
