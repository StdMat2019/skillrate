from typing import List
from uuid import UUID

from fastapi.security import OAuth2PasswordBearer
from fastapi import Depends, HTTPException, status
from jose import jwt
from jose.exceptions import JWTError
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import Teacher
from ..auth.hash_passwd import bcrypt

#ВЫНЕСТИ ВЫНЕСТИ ВЫНЕСТИ ВЫНЕСТИ ВЫНЕСТИ
oauth2_scheme = OAuth2PasswordBearer(tokenUrl='token')
SECRET_KEY = '52367badbf4e42f3a94d9ce456e1f01cbfee36a604da5c9589fa84f0bb9e661b'  # вынести в env


class TeacherCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_teacher(self, teacher_data: dict) -> Teacher:
        # Проверка наличия ключевых полей
        required_fields = ["name", "login", "hashed_passwd"]
        for field in required_fields:
            if field not in teacher_data or not teacher_data[field]:
                raise ValueError(f"{field} is required.")

        if len(teacher_data["name"]) > 20:
            raise ValueError("Name is too long.")
        teacher_data["hashed_passwd"] = bcrypt(teacher_data["hashed_passwd"])
        new_teacher = Teacher(**teacher_data)
        self.db.add(new_teacher)
        await self.db.commit()
        await self.db.refresh(new_teacher)
        return new_teacher

    async def create_teacher_class(self, teacher: Teacher) -> Teacher:
        # Проверка наличия ключевых полей
        required_fields = ["name", "login", "hashed_passwd"]
        for field in required_fields:
            if not getattr(teacher, field, None):
                raise ValueError(f"{field} is required.")

        if len(teacher.name) > 20:
            raise ValueError("Name is too long.")
        teacher.hashed_passwd = bcrypt(teacher.hashed_passwd)
        self.db.add(teacher)
        await self.db.commit()
        await self.db.refresh(teacher)
        return teacher

    async def get_teacher_by_id(self, teacher_id: UUID) -> Teacher:
        if not isinstance(teacher_id, UUID):
            raise ValueError("Invalid teacher_id type. Expected UUID.")

        result = await self.db.execute(select(Teacher).filter(Teacher.teacher_id == teacher_id))
        return result.scalars().first()

    async def get_teacher_by_name(self, name: str) -> Teacher:
        result = await self.db.execute(select(Teacher).filter(Teacher.name == name))
        return result.scalars().first()

    async def get_teacher(self, login: str):
        result = await self.db.execute(select(Teacher).filter(Teacher.login == login))
        return result.scalars().first()

    async def get_all_teachers(self) -> List[Teacher]:
        result = await self.db.execute(select(Teacher))
        return result.scalars().all()

    async def update_teacher(self, teacher_id: UUID, update_data: dict) -> Teacher:
        if not isinstance(teacher_id, UUID):
            raise ValueError("Invalid teacher_id type. Expected UUID.")

        result = await self.db.execute(select(Teacher).filter(Teacher.teacher_id == teacher_id))
        teacher = result.scalars().first()
        if teacher:
            for key, value in update_data.items():
                setattr(teacher, key, value)
            await self.db.commit()
            await self.db.refresh(teacher)
        return teacher

    async def delete_teacher(self, teacher_id: UUID) -> None:
        if not isinstance(teacher_id, UUID):
            raise ValueError("Invalid teacher_id type. Expected UUID.")

        result = await self.db.execute(select(Teacher).filter(Teacher.teacher_id == teacher_id))
        teacher = result.scalars().first()
        if teacher:
            await self.db.delete(teacher)
            await self.db.commit()

    async def get_current_teacher(self, token: str = Depends(oauth2_scheme)):
        credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
        try:
            if not token:
                return None
            payload = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
            login: str = payload.get("login")
            if login is None:
                raise credentials_exception
        except JWTError:
            raise credentials_exception
        user = await self.get_teacher(login)
        if user is None:
            raise credentials_exception

        return user
