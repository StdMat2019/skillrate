from uuid import UUID
from typing import List

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..database.models import TeacherCourse
from ..database.models import Course
from ..database.models import Teacher


class TeacherCourseCRUD:
    def __init__(self, db_session: AsyncSession):
        self.db = db_session

    async def create_teacher_course(self, teacher_course_data: dict) -> TeacherCourse:
        required_fields = ["teacher_id", "course_id", "role"]
        for field in required_fields:
            if field not in teacher_course_data or not teacher_course_data[field]:
                raise ValueError(f"{field} is required.")

        new_teacher_course = TeacherCourse(**teacher_course_data)
        self.db.add(new_teacher_course)
        await self.db.commit()
        await self.db.refresh(new_teacher_course)
        return new_teacher_course

    async def create_teacher_course_class(self, teacher_course: TeacherCourse) -> TeacherCourse:
        # Проверка наличия ключевых полей
        required_fields = ["teacher_id", "course_id"]
        for field in required_fields:
            if not getattr(teacher_course, field, None):
                raise ValueError(f"{field} is required.")

        # Получение связанных объектов Teacher и Course
        teacher = await self.db.execute(select(Teacher).filter(Teacher.teacher_id == teacher_course.teacher_id))
        course = await self.db.execute(select(Course).filter(Course.course_id == teacher_course.course_id))

        # Проверка наличия связанных объектов
        if not teacher.scalar() or not course.scalar():
            raise ValueError("Invalid teacher_id or course_id. Teacher or Course not found.")

        # Добавление и сохранение новой TeacherCourse
        self.db.add(teacher_course)
        await self.db.commit()
        await self.db.refresh(teacher_course)

        return teacher_course
    async def get_teacher_course_by_ids(self, teacher_id: UUID, course_id: UUID) -> TeacherCourse:
        result = await self.db.execute(
            select(TeacherCourse)
            .filter(TeacherCourse.teacher_id == teacher_id)
            .filter(TeacherCourse.course_id == course_id)
        )
        return result.scalars().first()

    async def get_all_teacher_course(self) -> List[TeacherCourse]:
        result = await self.db.execute(select(TeacherCourse))
        return result.scalars().all()

    async def get_all_teacher_courses(self, teacher_id: UUID) -> List[TeacherCourse]:
        result = await self.db.execute(
            select(TeacherCourse)
            .filter(TeacherCourse.teacher_id == teacher_id))
        return result.scalars().all()

    async def update_teacher_course(self, teacher_id: UUID, course_id: UUID, update_data: dict) -> TeacherCourse:
        result = await self.db.execute(
            select(TeacherCourse)
            .filter(TeacherCourse.teacher_id == teacher_id)
            .filter(TeacherCourse.course_id == course_id))
        teacher_course = result.scalars().first()
        if teacher_course:
            for key, value in update_data.items():
                setattr(teacher_course, key, value)
            await self.db.commit()
            await self.db.refresh(teacher_course)
        return teacher_course

    async def delete_teacher_course(self, teacher_id: UUID, course_id: UUID) -> None:
        result = await self.db.execute(
            select(TeacherCourse)
            .filter(TeacherCourse.teacher_id == teacher_id)
            .filter(TeacherCourse.course_id == course_id)
        )
        teacher_course = result.scalars().first()
        if teacher_course:
            await self.db.delete(teacher_course)
            await self.db.commit()
