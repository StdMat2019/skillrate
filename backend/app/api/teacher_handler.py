import uuid

from fastapi import APIRouter
from fastapi import Depends
from fastapi import Request
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from loguru import logger
from sqlalchemy.orm import Session

from ..crud.course_crud import CourseCRUD
from ..crud.course_group_crud import CourseGroupCRUD
from ..crud.student_course import StudentCourseCRUD
from ..crud.student_crud import StudentCRUD
from ..crud.test_stat_crud import TestStatCRUD
from ..crud.teacher_course import TeacherCourseCRUD
from ..crud.teacher_crud import TeacherCRUD
from ..crud.test_group_course import TestGroupCourseCRUD
from .user_handler import calculate_mark
teacher_router = APIRouter()
from ..database.database import get_db

teacher_router.mount("/pages", StaticFiles(directory="/app/frontend/src"), name="pages")
templates = Jinja2Templates(directory="/app/frontend/src/")


@teacher_router.get("/subjects")
async def teacher_sub(request: Request, database: Session = Depends(get_db)):
    logger.info("stat")
    access_token = request.cookies.get('access_token')
    crud_teacher = TeacherCRUD(database)
    auth_user = await crud_teacher.get_current_teacher(access_token)

    if not auth_user:
        return templates.TemplateResponse("index.html",
                                          {"request": request, "alert_msg": "Student is not authorized"},
                                          status_code=400)
    crud_teacher_course = TeacherCourseCRUD(database)
    teacher_courses = await crud_teacher_course.get_all_teacher_courses(auth_user.teacher_id)
    crud_course = CourseCRUD(database)
    courses_list = []
    for course in teacher_courses:
        course_info = await crud_course.get_course_by_id(course.course_id)
        courses_list.append(course_info)
    return templates.TemplateResponse("./pages/teacher/subjects.html", {"request": request,
                                                                        "courses_list": courses_list})


@teacher_router.get("/admin")
async def teacher_admin(request: Request):
    logger.info("stat")
    return templates.TemplateResponse("./pages/teacher/admin.html", {"request": request})


@teacher_router.get("/subjects/{course_id}")
async def get_groups(request: Request, course_id: str, database: Session = Depends(get_db)):
    logger.info("subjects_groups")
    access_token = request.cookies.get('access_token')
    crud_teacher = TeacherCRUD(database)
    auth_user = await crud_teacher.get_current_teacher(access_token)

    if not auth_user:
        return templates.TemplateResponse("index.html",
                                          {"request": request, "alert_msg": "Student is not authorized"},
                                          status_code=400)

    crud_courses = CourseGroupCRUD(database)
    groups_course = await crud_courses.get_course_by_course_id(course_id)
    groups_list = []
    for row in groups_course:
        group_dict = {"course_name": row["Course"].name, "avg_score": row["CourseGroup"].avg_score,
                      "avg_score_last_test": row["CourseGroup"].avg_score_last_test,
                      "is_start": row["CourseGroup"].is_start, "is_end": row["CourseGroup"].is_end,
                      "group_name": row["Group"].name, "group_id": row["Group"].group_id,
                      "course_id": row["Course"].course_id}
        groups_list.append(group_dict)
    return templates.TemplateResponse("./pages/teacher/tests.html",
                                      {"request": request, "groups_list": groups_list})


@teacher_router.get("/subjects/{course_id}/{group_id}")
async def get_groups(request: Request, course_id: str, group_id: str, database: Session = Depends(get_db)):
    logger.info("subjects_group_course")
    crud_student = StudentCRUD(database)
    students_obj_list = await crud_student.get_students_by_group_id(uuid.UUID(group_id))
    # Список студентов в группе
    students_id_in_group = {student_obj.student_id: student_obj.name for student_obj in students_obj_list}

    # crud_student_course = StudentCourseCRUD(database)
    # # какие из выбранных студентов в группе изучают предмет
    # course_members = await crud_student_course.get_all_by_students_id_group_id(list(students_id_in_group.keys()),
    #                                                                   uuid.UUID(course_id))

    crud_test_group_course = TestGroupCourseCRUD(database)
    crud_test_stat = TestStatCRUD(database)
    tests_stats = await crud_test_stat.get_test_stats_by_course_id(uuid.UUID(course_id))
    # тесты добавленные в рамках курса(предмета)
    test_list_obj = await crud_test_group_course.get_test_group_course_by_id(uuid.UUID(course_id))
    collection_names = {test_obj.test_group_course_id: test_obj.collection_name for test_obj in test_list_obj}
    test_ids = {test_obj.collection_name: test_obj.test_group_course_id for test_obj in test_list_obj}
    # таблица для успеваемости на курсе(предмете)
    result_table = {}

    # алгоритм заполнения таблицы
    for test in test_list_obj:
        result_table[test.test_group_course_id] = {}
        # for student_item in students_id_in_group.items():
        #     # 0 по умолчанию для всх не решенных тестов
        #     result_table[test.test_group_course_id][student_item[0]] = 0

    for test_result in tests_stats:
        # тест принадлежит студенту
        if test_result.student_id in students_id_in_group.keys():
            logger.info(test_result.get_dict())
            result_table[test_ids[test_result.collection_name]][test_result.student_id] = \
                (calculate_mark(test_result.successful_points, test_result.total_points))

    logger.info(result_table)
    return templates.TemplateResponse("./pages/teacher/group_course_statistic.html",
                                      {"request": request, "result_table": result_table,
                                       "names_dict": students_id_in_group, "collection_names": collection_names})
