import datetime
import json
import uuid
from typing import List

import httpx
from fastapi import APIRouter
from fastapi import Depends
from fastapi import Form
from fastapi import Request
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from loguru import logger
from sqlalchemy.orm import Session

from ..crud.course_crud import CourseCRUD
from ..crud.quest_stat_crud import QuestStatCRUD
from ..crud.skill_crud import SkillCRUD
from ..crud.student_course import StudentCourseCRUD
from ..crud.student_crud import StudentCRUD
from ..crud.student_skill import StudentSkillCRUD
from ..crud.test_group_course import TestGroupCourseCRUD
from ..crud.test_stat_crud import TestStatCRUD
from ..database.database import get_db
from ..database.models import QuestStat
from ..database.models import StudentSkill
from ..database.models import TestStat

user_router = APIRouter()
templates = Jinja2Templates(directory="/app/frontend/src/")
user_router.mount("/pages", StaticFiles(directory="/app/frontend/src"), name="pages")


def calculate_grade(total_points, successful_points):
    if total_points == 0:
        return "НЕУД"
    res = (successful_points / total_points) * 100
    if res > 90:
        return "ОТЛ"
    elif res > 70:
        return "ХОР"
    elif res > 50:
        return "УДОВЛ"
    else:
        return "НЕУД"


def calculate_mark(successful_points, total_points):
    if total_points == 0:
        return 2
    res = (successful_points / total_points) * 100
    if res > 90:
        return 5
    elif res > 70:
        return 4
    elif res > 50:
        return 3
    else:
        return 2


@user_router.get("/stat")
async def student_stat(request: Request,
                       database: Session = Depends(get_db)):
    logger.info("stat")
    access_token = request.cookies.get('access_token')
    crud_stud = StudentCRUD(database)

    auth_user = await crud_stud.get_current_student(access_token)
    if not auth_user:
        return templates.TemplateResponse("index.html",
                                          {"request": request, "alert_msg": "Student is not authorized"},
                                          status_code=400)

    crud_test = TestStatCRUD(database)
    all_tests = await crud_test.get_test_stats_by_stud_id(auth_user.student_id, 5)
    tests_dicts = [test.get_dict() for test in all_tests]
    for test_res in tests_dicts:
        del test_res["test_id"]
        del test_res["student_id"]
        del test_res["date"]
        del test_res["total_time"]
        crud_course = CourseCRUD(database)
        course = await crud_course.get_course_by_id(uuid.UUID(test_res["course_id"]))
        test_res["course_id"] = course.name
        test_res["color"] = course.color
        test_res["percent"] = int(test_res["successful_points"]) / int(test_res["total_points"]) * 100
        test_res["grade"] = calculate_grade(test_res["total_points"], test_res["successful_points"])
    crud_stud_skills = StudentSkillCRUD(database)
    crud_skills = SkillCRUD(database)
    skill_dict = await crud_stud_skills.get_all_student_skills_by_id(auth_user.student_id, 5)
    skill_dicts = [skill.get_dict() for skill in skill_dict]
    for skill_res in skill_dicts:
        del skill_res["student_id"]
        skill_obj = await crud_skills.get_skill_by_id(uuid.UUID(skill_res["skill_id"]))
        skill_res["skill_id"] = skill_obj.name
        skill_res["percent"] = int((skill_res["successful_points"] / skill_res["total_points"] * 100))
    # logger.info(auth_user)
    return templates.TemplateResponse("./pages/student/statistic.html", {"request": request,
                                                                         "auth_user": auth_user.get_dict(),
                                                                         "tests_dicts": tests_dicts,
                                                                         "skill_dicts": skill_dicts})


@user_router.get("/subjects")
async def student_sub(request: Request,
                      database: Session = Depends(get_db)):
    logger.info("subjects")
    access_token = request.cookies.get('access_token')
    crud_stud = StudentCRUD(database)
    crud_course = CourseCRUD(database)

    auth_user = await crud_stud.get_current_student(access_token)
    if not auth_user:
        return templates.TemplateResponse("index.html",
                                          {"request": request, "alert_msg": "Student is not authorized"},
                                          status_code=400)
    crud_student_course = StudentCourseCRUD(database)
    courses = await crud_student_course.get_all_by_student_id(auth_user.student_id)
    courses_dicts = [course.get_dict() for course in courses]
    for course in courses_dicts:
        course_obj = await crud_course.get_course_by_id(uuid.UUID(course["course_id"]))
        course["course_fullname"] = course_obj.fullname
        course["color"] = course_obj.color
        del course["student_id"]
    return templates.TemplateResponse("./pages/student/subjects.html", {"request": request,
                                                                        "courses_dicts": courses_dicts})


@user_router.get("/subjects/{course_id}")
async def student_sub(request: Request, course_id: str,
                      database: Session = Depends(get_db)):
    access_token = request.cookies.get('access_token')
    crud_stud = StudentCRUD(database)
    crud_test_stat = TestStatCRUD(database)
    crud_test_group_course = TestGroupCourseCRUD(database)
    auth_user = await crud_stud.get_current_student(access_token)
    if not auth_user:
        return templates.TemplateResponse("index.html",
                                          {"request": request, "alert_msg": "Student is not authorized"},
                                          status_code=400)
    solved_tests = await crud_test_stat.get_test_stats_by_stud_id_teacher_id(auth_user.student_id, uuid.UUID(course_id))
    all_tests = await crud_test_group_course.get_test_group_course_by_id(uuid.UUID(course_id))

    solved_dicts = [test.get_dict() for test in solved_tests]
    all_tests_dicts = [test.get_dict() for test in all_tests]
    for test in solved_dicts:
        test["grade"] = calculate_grade(test["total_points"], test["successful_points"])
        del test["test_id"]
        del test["student_id"]
        del test["date"]
        del test["total_time"]
        iterat = 0
        for test_all in all_tests_dicts:
            if test["collection_name"] == test_all["collection_name"]:
                del all_tests_dicts[iterat]
            iterat += 1

    return templates.TemplateResponse("./pages/student/tests.html", {"request": request,
                                                                     "tests_dicts": solved_dicts,
                                                                     "all_tests_dicts": all_tests_dicts})


async def post_test_request(collection_name: str, skills_list: dict, all_skills: list, asked_questions: dict):
    async with httpx.AsyncClient() as client:
        url = "http://test_skillrate_backend:8000/get_test"
        payload = collection_name, skills_list, all_skills, asked_questions
        response = await client.post(url, json=payload)
        return response.text


async def post_check_res_request(id_answers_dict: dict, questions_id_list: list, collection_name: str):
    async with httpx.AsyncClient() as client:
        url = "http://test_skillrate_backend:8000/check_results"
        payload = id_answers_dict, questions_id_list, collection_name
        logger.info(payload)
        response = await client.post(url, json=payload)
        return response.text


@user_router.get("/subjects/get_test/{course_id}/{collection_name}")
async def get_test(request: Request, course_id: str, collection_name: str, database: Session = Depends(get_db)):
    access_token = request.cookies.get('access_token')
    crud_stud = StudentCRUD(database)
    auth_user = await crud_stud.get_current_student(access_token)
    if not auth_user:
        return templates.TemplateResponse("index.html",
                                          {"request": request, "alert_msg": "Student is not authorized"},
                                          status_code=400)

    crud_stud_skill = StudentSkillCRUD(database)
    skills_list = await crud_stud_skill.get_all_student_skills_by_id(auth_user.student_id)
    crud_skill = SkillCRUD(database)
    all_skills = await crud_skill.get_all_skills()
    skill_pairs = {}
    for skill in all_skills:
        skill_pairs = skill_pairs | skill.get_dict_skill_id_name()
    skills_dict_list = [skill.get_dict() for skill in skills_list]
    successful_sum = 0
    for skill in skills_dict_list:
        successful_sum += skill["successful_points"]
    skills_dict = {}
    for skill in skills_dict_list:
        if skill["skill_id"] in skill_pairs.keys():
            skill["name"] = skill_pairs[skill["skill_id"]]
            skills_dict[skill["name"]] = round(1 - skill["successful_points"] / successful_sum, 2)
    crud_quest = QuestStatCRUD(database)
    asked_questions = await crud_quest.get_quest_stat_by_student(auth_user.student_id)
    asked_dict = {}
    for question in asked_questions:
        asked_dict = asked_dict | question.get_dict_shot()
    questions = await post_test_request(collection_name.strip(), skills_dict, list(skill_pairs.values()),
                                        asked_dict)

    questions_dict = json.loads(questions)

    if questions_dict is None:
        return {"Недостаточное число непройденных вопросов"}
    return templates.TemplateResponse("./pages/student/solve_test.html",
                                      {"request": request, "questions": questions_dict,
                                       "test_fullname": questions_dict[0]["test_fullname"],
                                       "collection_name": collection_name.strip(),
                                       "course_id": course_id})


@user_router.post("/subjects/check_test")
async def student_test(request: Request, answers: List[str] = Form(...), questions_id_list: list = Form(...),
                       collection_name: str = Form(...), course_id: str = Form(...),
                       database: Session = Depends(get_db)):
    # преобразование списка в формате str в dict
    # Удаление лишних символов и разбор строки по запятым
    pairs = questions_id_list[0].replace("'", "").split(',')
    skills_dict = {}
    # Проход по каждой паре и добавление в словарь
    for pair in pairs:
        key, value = pair.strip().split('+')
        skills_dict[key.strip()] = value.strip().rstrip('+')

    logger.info(skills_dict)

    access_token = request.cookies.get('access_token')
    crud_stud = StudentCRUD(database)
    auth_user = await crud_stud.get_current_student(access_token)
    if not auth_user:
        return templates.TemplateResponse("index.html",
                                          {"request": request, "alert_msg": "Student is not authorized"},
                                          status_code=400)
    #  answers список пар questions_id и ответ
    questions_id_list = skills_dict.keys()

    logger.info(questions_id_list)
    logger.info("check_test")
    # Словарь, где ключ - это question_id, а значение - это список ответов
    id_answers_dict = {}
    for answer in answers:
        question_id, text_answer = answer.split(" ", 1)
        if question_id not in id_answers_dict:
            id_answers_dict[question_id] = []
        id_answers_dict[question_id].append(text_answer)
    # Отправка на сервер тестов
    test_results = await post_check_res_request(id_answers_dict, list(questions_id_list), collection_name)
    test_results_dict = json.loads(test_results)

    #  Учет полученных результатов
    mark = calculate_mark(test_results_dict["total_res_quest"], test_results_dict["total_quest_score"])
    crud_test_stat = TestStatCRUD(database)
    crud_quest_stat = QuestStatCRUD(database)
    test_id = uuid.uuid4()
    stat = TestStat(student_id=auth_user.student_id,
                    course_id=course_id,
                    test_id=test_id,
                    collection_name=collection_name,
                    date=datetime.datetime.now(),
                    total_time="15:00",
                    total_points=test_results_dict["total_quest_score"],
                    successful_points=test_results_dict["total_res_quest"])
    # logger.info(stat.get_dict())
    await crud_test_stat.create_test_stat_class(stat)
    # Добавление в quest_stat необходимо для учета отвеченных вопросов
    for question in test_results_dict["question_score_dict"].items():
        quest = QuestStat(test_id=test_id,
                          student_id=auth_user.student_id,
                          question_id=question[0],
                          total_points=question[1][1],
                          successful_points=round(question[1][0], 1),
                          old_result=False)
        await crud_quest_stat.create_quest_stat_class(quest)

    # словарь вида {навык:(всего баллов, получено баллов)}
    res_skill_dict = {}
    # сначала происходит предобработка чтобы снизить число запросов к бд
    for quest_id, skill in skills_dict.items():
        if skill in res_skill_dict:
            #  В существующую пару добавляются значение total_points и successful_points из нового вопроса
            res_skill_dict[skill] = res_skill_dict[skill][0] + test_results_dict["question_score_dict"][quest_id][0], \
                                    res_skill_dict[skill][1] + test_results_dict["question_score_dict"][quest_id][1]
        else:
            res_skill_dict[skill] = test_results_dict["question_score_dict"][quest_id][0], \
                test_results_dict["question_score_dict"][quest_id][1]

    # Добавление в student_skill необходимо для ведения статистики по навыкам
    crud_skill = SkillCRUD(database)
    crud_stud_skill = StudentSkillCRUD(database)
    for skill, points in res_skill_dict.items():
        logger.info(skill)
        skill_obj = await crud_skill.get_skill_by_name(skill.lower())

        new_stud_skill = StudentSkill(student_id=auth_user.student_id,
                                      skill_id=skill_obj.skill_id,
                                      total_points=points[1],
                                      successful_points=points[0])
        logger.info(new_stud_skill.get_dict())
        await crud_stud_skill.create_student_skill_class(new_stud_skill)

    return templates.TemplateResponse("./pages/student/test_result.html",
                                      {"request": request, "total_res_quest": test_results_dict["total_res_quest"],
                                       "total_quest_score": test_results_dict["total_quest_score"], "mark": mark,
                                       "question_score_dict": test_results_dict["question_score_dict"]})
