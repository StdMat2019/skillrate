import io
import json
import uuid
from datetime import datetime
from datetime import timedelta

import httpx
import pandas as pd
from fastapi import APIRouter
from fastapi import Depends
from fastapi import File
from fastapi import Form
from fastapi import Request
from fastapi import UploadFile
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from loguru import logger
from sqlalchemy.orm import Session

from .user_handler import calculate_mark
from ..crud.course_crud import CourseCRUD
from ..crud.course_group_crud import CourseGroupCRUD
from ..crud.course_skill import CourseSkillCRUD
from ..crud.course_speciality import CourseSpecialityCRUD
from ..crud.group_crud import GroupCRUD
from ..crud.kathedra_crud import KathedraCRUD
from ..crud.quest_stat_crud import QuestStatCRUD
from ..crud.role_crud import RoleCRUD
from ..crud.skill_crud import SkillCRUD
from ..crud.speciality_crud import SpecialityCRUD
from ..crud.student_course import StudentCourseCRUD
from ..crud.student_crud import StudentCRUD
from ..crud.student_skill import StudentSkillCRUD
from ..crud.teacher_course import TeacherCourseCRUD
from ..crud.teacher_crud import TeacherCRUD
from ..crud.test_group_course import TestGroupCourseCRUD
from ..crud.test_stat_crud import TestStatCRUD
from ..database.database import get_db
from ..database.models import Course
from ..database.models import CourseGroup
from ..database.models import CourseSkill
from ..database.models import CourseSpeciality
from ..database.models import Group
from ..database.models import Kathedra
from ..database.models import QuestStat
from ..database.models import Role
from ..database.models import Skill
from ..database.models import Speciality
from ..database.models import Student
from ..database.models import StudentCourse
from ..database.models import StudentSkill
from ..database.models import Teacher
from ..database.models import TeacherCourse
from ..database.models import TestGroupCourse
from ..database.models import TestStat

admin_router = APIRouter()
admin_router.mount("/pages", StaticFiles(directory="/app/frontend/src"), name="pages")

templates = Jinja2Templates(directory="/app/frontend/src/")


@admin_router.get("/")
async def home(request: Request):
    logger.info("stat")
    return templates.TemplateResponse("./pages/admin/admin.html", {"request": request})


@admin_router.get("/add_student")
async def add_student(request: Request):
    logger.info("add_student")
    return templates.TemplateResponse("./pages/admin/add/add_student.html", {"request": request})


@admin_router.get("/add_role")
async def add_role(request: Request):
    logger.info("add_role")
    return templates.TemplateResponse("./pages/admin/add/add_role.html", {"request": request})


@admin_router.get("/add_speciality")
async def add_speciality(request: Request):
    logger.info("add_speciality")
    return templates.TemplateResponse("./pages/admin/add/add_speciality.html", {"request": request})


@admin_router.get("/add_group")
async def add_group(request: Request):
    logger.info("add_group")
    return templates.TemplateResponse("./pages/admin/add/add_group.html", {"request": request})


@admin_router.get("/add_course")
async def add_course(request: Request):
    logger.info("add_course")
    return templates.TemplateResponse("./pages/admin/add/add_course.html", {"request": request})


@admin_router.get("/add_kathedra")
async def add_kathedra(request: Request):
    logger.info("add_kathedra")
    return templates.TemplateResponse("./pages/admin/add/add_kathedra.html", {"request": request})


@admin_router.get("/add_skill")
async def add_skill(request: Request):
    logger.info("add_skill")
    return templates.TemplateResponse("./pages/admin/add/add_skill.html", {"request": request})


@admin_router.get("/add_teacher")
async def add_teacher(request: Request):
    logger.info("add_teacher")
    return templates.TemplateResponse("./pages/admin/add/add_teacher.html", {"request": request})


@admin_router.get("/add_test_stat")
async def add_test_stat(request: Request):
    logger.info("add_test_stat")
    return templates.TemplateResponse("./pages/admin/add/add_test_stat.html", {"request": request})


@admin_router.get("/add_course_speciality")
async def add_course_speciality(request: Request):
    logger.info("add_course_speciality")
    return templates.TemplateResponse("./pages/admin/add/add_course_speciality.html", {"request": request})


@admin_router.get("/add_course_speciality")
async def add_course_speciality(request: Request):
    logger.info("add_course_speciality")
    return templates.TemplateResponse("./pages/admin/add/add_course_skill.html", {"request": request})


@admin_router.get("/add_teacher_course")
async def add_teacher_course(request: Request):
    logger.info("add_teacher_course")
    return templates.TemplateResponse("./pages/admin/add/add_teacher_course.html", {"request": request})


@admin_router.get("/add_course_group")
async def add_course_group(request: Request):
    logger.info("add_course_group")
    return templates.TemplateResponse("./pages/admin/add/add_course_group.html", {"request": request})


@admin_router.get("/add_student_course")
async def add_student_course(request: Request):
    logger.info("add_student_course")
    return templates.TemplateResponse("./pages/admin/add/add_student_course.html", {"request": request})


@admin_router.get("/add_student_skill")
async def add_student_skill(request: Request):
    logger.info("add_student_course")
    return templates.TemplateResponse("./pages/admin/add/add_student_skill.html", {"request": request})


@admin_router.get("/add_test_group_course")
async def add_test_group_course(request: Request):
    logger.info("add_test_group_course")
    return templates.TemplateResponse("./pages/admin/add/add_test_group_course.html", {"request": request})


@admin_router.get("/add_question_stat")
async def add_question_stat(request: Request):
    logger.info("add_question_stat")
    return templates.TemplateResponse("./pages/admin/add/add_question_stat.html", {"request": request})


@admin_router.get("/add_excel")
async def add_student_group(request: Request):
    logger.info("add_excel")
    return templates.TemplateResponse("./pages/admin/add/add_excel.html", {"request": request})


@admin_router.post("/add_student")
async def add_student_post(request: Request,
                           name: str = Form(...),
                           passwd: str = Form(...),
                           login: str = Form(...),
                           telegram: str = Form(None),
                           fullname: str = Form(None),
                           name_rp: str = Form(None),
                           gitlab: str = Form(None),
                           gitwork: str = Form(None),
                           years_over: int = Form(None),
                           completed_courses: int = Form(None),
                           total_score: int = Form(None),
                           notice: str = Form(None),
                           group_id: uuid.UUID = Form(None),
                           role_id: uuid.UUID = Form(None),
                           database: Session = Depends(get_db)):
    logger.info("try add student")
    # проверка на возможность создания
    new_student = Student(name=name, login=login, gitlab=gitlab, role_id=role_id,
                          hashed_passwd=passwd, telegram=telegram, name_rp=name_rp,
                          years_over=years_over, completed_courses=completed_courses,
                          total_score=total_score, gitwork=gitwork, notice=notice,
                          group_id=group_id, fullname=fullname)
    crud = StudentCRUD(database)
    await crud.create_student_class(new_student)

    return templates.TemplateResponse("./pages/admin/add/add_student.html", {"request": request})


@admin_router.post("/add_speciality")
async def add_speciality_post(request: Request,
                              name: str = Form(...),
                              fullname: str = Form(...),
                              number: int = Form(...),
                              database: Session = Depends(get_db)):
    logger.info("add speciality")
    # проверка на возможность создания обработка значения не int
    new_speciality = Speciality(number=int(number), name=name, fullname=fullname)
    crud = SpecialityCRUD(database)
    await crud.create_speciality_class(new_speciality)

    return templates.TemplateResponse("./pages/admin/add/add_speciality.html", {"request": request})


@admin_router.post("/add_role")
async def add_role_post(request: Request,
                        role: str = Form(...),
                        database: Session = Depends(get_db)):
    logger.info("add role")

    # проверка на возможность создания
    new_role = Role(name=role)

    crud = RoleCRUD(database)
    await crud.create_role_class(new_role)

    return templates.TemplateResponse("./pages/admin/add/add_role.html", {"request": request})


@admin_router.post("/add_group")
async def add_group_post(request: Request,
                         name: str = Form(...),
                         start_date_str: str = Form(...),
                         end_date_str: str = Form(...),
                         speciality_number: str = Form(...),
                         count_members: int = Form(0),
                         database: Session = Depends(get_db)):
    logger.info("add group")
    # проверка на int
    speciality_id = await SpecialityCRUD(database).get_speciality_id_by_number(int(speciality_number))
    id_name = await SpecialityCRUD(database).get_speciality_id_by_name(speciality_number)

    if speciality_id is None:
        speciality_id = id_name
    if speciality_id is None:
        logger.info("name or number group not exists")
        return templates.TemplateResponse("./pages/admin/add/add_group.html", {"request": request})
    start_date = datetime.strptime(start_date_str, "%d-%m-%Y").date()
    end_date = datetime.strptime(end_date_str, "%d-%m-%Y").date()

    # проверка на возможность создания
    new_group = Group(name=name, start_date=start_date, end_date=end_date,
                      speciality_id=speciality_id, count_members=count_members)

    crud = GroupCRUD(database)
    await crud.create_group_class(new_group)

    return templates.TemplateResponse("./pages/admin/add/add_group.html", {"request": request})


@admin_router.post("/add_course")
async def add_course_post(request: Request,
                          name: str = Form(...),
                          version: float = Form(...),
                          avg_score: float = Form(...),
                          color: str = Form(...),
                          fullname: str = Form(...),
                          database: Session = Depends(get_db)):
    logger.info("add course")

    # проверка на возможность создания
    new_course = Course(name=name, version=version, avg_score=avg_score, color=color, fullname=fullname)

    crud = CourseCRUD(database)
    await crud.create_course_class(new_course)

    return templates.TemplateResponse("./pages/admin/add/add_course.html", {"request": request})


@admin_router.post("/add_kathedra")
async def add_kathedra_post(request: Request,
                            name: str = Form(...),
                            fullname: str = Form(...),
                            database: Session = Depends(get_db)):
    logger.info("add kathedra")

    # проверка на возможность создания
    new_kathedra = Kathedra(name=name, fullname=fullname)

    crud = KathedraCRUD(database)
    await crud.create_kathedra_class(new_kathedra)

    return templates.TemplateResponse("./pages/admin/add/add_kathedra.html", {"request": request})


@admin_router.post("/add_teacher")
async def add_teacher_post(request: Request,
                           name: str = Form(...),
                           gitlab: str = Form(None),
                           gitwork: str = Form(None),
                           login: str = Form(...),
                           kathedra_name: str = Form(...),
                           hashed_passwd: str = Form(...),
                           telegram: str = Form(None),
                           database: Session = Depends(get_db)):
    logger.info("add teacher")
    kathedra_obj = await KathedraCRUD(database).get_kathedra_id_by_name(kathedra_name)

    if kathedra_obj is None:
        logger.info("Kathedra not exists")
        return templates.TemplateResponse("./pages/admin/add/add_teacher.html", {"request": request})

    # проверка на возможность создания
    new_teacher = Teacher(name=name, login=login, kathedra_id=kathedra_obj.kathedra_id,
                          hashed_passwd=hashed_passwd, gitlab=gitlab,
                          gitwork=gitwork, telegram=telegram)

    crud = TeacherCRUD(database)
    await crud.create_teacher_class(new_teacher)

    return templates.TemplateResponse("./pages/admin/add/add_teacher.html", {"request": request})


@admin_router.post("/add_skill")
async def add_skill_post(request: Request,
                         name: str = Form(...),
                         database: Session = Depends(get_db)):
    logger.info("add skill")

    # проверка на возможность создания
    new_skill = Skill(name=name)

    crud = SkillCRUD(database)
    await crud.create_skill_class(new_skill)

    return templates.TemplateResponse("./pages/admin/add/add_skill.html", {"request": request})


@admin_router.post("/add_test_stat")
async def add_test_stat_post(request: Request,
                             student_id: str = Form(...),
                             course_id: str = Form(...),
                             test_group_course_id: str = Form(...),
                             collection_name: str = Form(...),
                             date: str = Form(...),
                             total_time: str = Form(...),
                             total_points: float = Form(...),
                             successful_points: float = Form(...),
                             database: Session = Depends(get_db)):
    logger.info("add test stat")
    date_obj = datetime.strptime(date, '%d-%m-%Y')
    mark = calculate_mark(successful_points, total_points)
    # проверка на возможность создания
    new_test_stat = TestStat(student_id=student_id, course_id=course_id, date=date_obj,
                             collection_name=collection_name, total_time=total_time,
                             total_points=total_points, successful_points=successful_points,
                             test_group_course_id=test_group_course_id, test_mark=mark)

    crud = TestStatCRUD(database)
    await crud.create_test_stat_class(new_test_stat)

    return templates.TemplateResponse("./pages/admin/add/add_test_stat.html", {"request": request})


@admin_router.post("/add_course_speciality")
async def add_course_speciality_post(request: Request,
                                     course_id: str = Form(...),
                                     speciality_id: str = Form(...),
                                     database: Session = Depends(get_db)):
    logger.info("add course-speciality")

    # проверка на возможность создания
    new_course_speciality = CourseSpeciality(course_id=course_id, speciality_id=speciality_id)

    crud = CourseSpecialityCRUD(database)
    await crud.create_course_speciality_class(new_course_speciality)

    return templates.TemplateResponse("./pages/admin/add/add_course_speciality.html", {"request": request})


@admin_router.post("/add_course_skill")
async def add_course_skill_post(request: Request,
                                course_id: str = Form(...),
                                skill_id: str = Form(...),
                                learn_require: str = Form(...),
                                database: Session = Depends(get_db)):
    logger.info("add course skill")

    # проверка на возможность создания
    new_course_speciality = CourseSkill(course_id=course_id, skill_id=skill_id, learn_require=learn_require)

    crud = CourseSkillCRUD(database)
    await crud.create_course_skill_class(new_course_speciality)

    return templates.TemplateResponse("./pages/admin/add/add_course_skill.html", {"request": request})


@admin_router.post("/add_teacher_course")
async def add_teacher_course_post(request: Request,
                                  course_id: str = Form(...),
                                  teacher_id: str = Form(...),
                                  role: str = Form(...),
                                  database: Session = Depends(get_db)):
    logger.info("add course teacher")

    # проверка на возможность создания
    new_course_speciality = TeacherCourse(course_id=course_id, teacher_id=teacher_id, role=role)

    crud = TeacherCourseCRUD(database)
    await crud.create_teacher_course_class(new_course_speciality)

    return templates.TemplateResponse("./pages/admin/add/add_teacher_course.html", {"request": request})


@admin_router.post("/add_course_group")
async def add_course_group_post(request: Request,
                                group_id: str = Form(...),
                                course_id: str = Form(...),
                                course_over: int = Form(...),
                                avg_score: float = Form(...),
                                is_end: str = Form(None),
                                is_start: str = Form(None),
                                avg_score_last_test: float = Form(...),
                                database: Session = Depends(get_db)):
    logger.info("add course group")
    is_end_value = bool(is_end)
    is_start_value = bool(is_start)
    # проверка на возможность создания
    new_course_group = CourseGroup(
        group_id=group_id,
        course_id=course_id,
        course_over=course_over,
        avg_score=avg_score,
        is_end=is_end_value,
        is_start=is_start_value,
        avg_score_last_test=avg_score_last_test
    )

    crud = CourseGroupCRUD(database)
    await crud.create_course_group_class(new_course_group)

    return templates.TemplateResponse("./pages/admin/add/add_course_group.html", {"request": request})


@admin_router.post("/add_test_group_course")
async def add_test_group_course_post(request: Request,
                                     course_id: str = Form(...),
                                     collection_name: str = Form(...),
                                     course_fullname: str = Form(...),
                                     is_open: str = Form(None),
                                     database: Session = Depends(get_db)):
    logger.info("add test group course")
    is_open_value = bool(is_open)
    # проверка на возможность создания
    new_test_group_course = TestGroupCourse(
        course_id=course_id,
        collection_name=collection_name,
        course_fullname=course_fullname,
        is_open=is_open_value
    )

    crud = TestGroupCourseCRUD(database)
    await crud.create_test_group_course_class(new_test_group_course)

    return templates.TemplateResponse("./pages/admin/add/add_test_group_course.html", {"request": request})


@admin_router.post("/add_test_group_course_mongo")
async def add_test_group_course_post(request: Request,
                                     collection_name: str = Form(...),
                                     test_fullname: str = Form(...),
                                     is_open: str = Form(None),
                                     skills_dict: str = Form(...),
                                     database: Session = Depends(get_db)):
    logger.info("add test group course mongo")
    is_open_value = bool(is_open.lower() == "true")
    # проверка на возможность создания
    crud_skills = SkillCRUD(database)
    crud_course_skill = CourseSkillCRUD(database)
    skills_dict = json.loads(skills_dict)
    crud = CourseCRUD(database)
    course_obj = await crud.get_course_by_name(collection_name.split(".")[0])

    new_test_group_course = TestGroupCourse(
        course_id=course_obj.course_id,
        collection_name=collection_name,
        test_fullname=test_fullname,
        is_open=is_open_value
    )
    for skill_name in skills_dict.keys():
        logger.info(skill_name)
        skill_obj = await crud_skills.get_skill_by_name(skill_name)
        if skill_obj is None:
            # Если навык(скилл) не существует в бд, но есть в тесте его нужно добавить в тест
            skill_obj = await crud_skills.create_skill_class(Skill(name=skill_name))
            new_course_skill = CourseSkill(course_id=course_obj.course_id, skill_id=skill_obj.skill_id)
            await crud_course_skill.create_course_skill_class(new_course_skill)
        course_skill_obj = await crud_course_skill.get_course_skill_by_ids(skill_id=skill_obj.skill_id,
                                                                           course_id=course_obj.course_id)
        if course_skill_obj is None:
            # Если есть навык, но его нет в предмет_навык
            new_course_skill = CourseSkill(course_id=course_obj.course_id, skill_id=skill_obj.skill_id)
            await crud_course_skill.create_course_skill_class(new_course_skill)

    crud = TestGroupCourseCRUD(database)
    res = await crud.create_test_group_course_class(new_test_group_course)

    return res


@admin_router.post("/add_student_course")
async def add_student_course_post(request: Request,
                                  course_id: str = Form(...),
                                  student_id: str = Form(...),
                                  result: float = Form(...),
                                  complete_percent: float = Form(...),
                                  avg_score: float = Form(...),
                                  database: Session = Depends(get_db)):
    # Проверка на возможность создания
    new_student_course = StudentCourse(
        course_id=course_id,
        student_id=student_id,
        result=result,
        complete_percent=complete_percent,
        avg_score=avg_score
    )

    crud = StudentCourseCRUD(database)
    await crud.create_student_course_class(new_student_course)

    return templates.TemplateResponse("./pages/admin/add/add_student_course.html", {"request": request})


@admin_router.post("/add_student_skill")
async def add_student_skill_post(request: Request,
                                 student_id: str = Form(...),
                                 total_points: float = Form(...),
                                 successful_points: float = Form(...),
                                 skill_id: str = Form(...),
                                 group_id: str = Form(...),
                                 database: Session = Depends(get_db)):
    logger.info("add student skill")

    # Проверка на возможность создания
    new_student_skill = StudentSkill(
        student_id=student_id,
        total_points=total_points,
        successful_points=successful_points,
        skill_id=skill_id,
        group_id=group_id
    )

    crud = StudentSkillCRUD(database)
    await crud.create_student_skill_class(new_student_skill)

    return templates.TemplateResponse("./pages/admin/add/add_student_skill.html", {"request": request})


@admin_router.post("/add_question_stat")
async def add_question_stat_post(request: Request,
                                 test_id: str = Form(...),
                                 student_id: str = Form(...),
                                 question_id: str = Form(...),
                                 total_points: float = Form(...),
                                 successful_points: float = Form(...),
                                 database: Session = Depends(get_db)):
    logger.info("Add question stat")

    # Проверка на возможность создания
    new_question_stat = QuestStat(
        test_id=test_id,
        student_id=student_id,
        question_id=question_id,
        total_points=total_points,
        successful_points=successful_points
    )

    crud = QuestStatCRUD(database)
    await crud.create_quest_stat_class(new_question_stat)

    return templates.TemplateResponse("./pages/admin/add/add_question_stat.html", {"request": request})


@admin_router.post("/add_student_group")
async def add_student_group(request: Request, file_group: UploadFile = File(...), database: Session = Depends(get_db)):
    logger.info("Add student group")
    content = await file_group.read()
    excel_file = io.BytesIO(content)
    df = pd.read_excel(excel_file)
    speciality, *group = str(file_group.filename).split("-")
    group_name = f"{group[0]}-{group[1].split('.')[0]}"
    crud_speciality = SpecialityCRUD(database)
    speciality_obj = await crud_speciality.get_speciality_id_by_name(speciality)
    # создание специальности есть не существует
    if speciality_obj is None:
        speciality_obj = await crud_speciality.create_speciality_class(Speciality(name=speciality))
        speciality_obj = speciality_obj.speciality_id

    crud_group = GroupCRUD(database)
    group_obj = await crud_group.get_group_by_name(group_name)
    # создание группы есть не существует
    if group_obj is None:
        current_date = datetime.now().date()
        future_date = current_date + timedelta(days=365 * 5)
        group_obj = await crud_group.create_group_class(Group(name=group_name,
                                                              start_date=current_date,
                                                              end_date=future_date,
                                                              speciality_id=speciality_obj,
                                                              count_members=0))

    df = df.applymap(lambda x: str(x) if not pd.isna(x) else None)  # Заполнение NaN значением None
    crud_stud = StudentCRUD(database)
    crud_role = RoleCRUD(database)
    for _, row in df.iterrows():
        # если роль не существует она создается
        role_obj = Role()
        if row['role']:
            role_obj = await crud_role.get_role_by_name(str(row['role']).lower())
            if role_obj is None:
                role_obj = await crud_role.create_role_class(Role(name=str(row['role']).lower()))

        else:
            role_obj = await crud_role.get_role_by_name("студент")
            if role_obj is None:
                role_obj = await crud_role.create_role_class(Role(name="студент"))

        new_student = Student(role_id=role_obj.role_id, name=row['ФИО'], fullname=row['fullname'],
                              login=str(row['login']), hashed_passwd=str(row['password']), gitlab=row['gitlab'],
                              gitwork=row['gitwork'], telegram=row['telegram'],
                              years_over=0, completed_courses=0, total_score=0, notice='', group_id=group_obj.group_id)
        await crud_stud.create_student_class(new_student)

    return templates.TemplateResponse("./pages/admin/add/add_excel.html", {"request": request})


@admin_router.post("/add_teacher_group_excel")
async def add_teacher_group_excel(request: Request, file_teacher: UploadFile = File(...),
                                  database: Session = Depends(get_db)):
    logger.info("Add teacher group")
    content = await file_teacher.read()
    excel_file = io.BytesIO(content)
    df = pd.read_excel(excel_file)
    crud_katedra = KathedraCRUD(database)
    crud_teacher = TeacherCRUD(database)
    df = df.applymap(lambda x: str(x) if not pd.isna(x) else None)  # Заполнение NaN значением None
    for _, row in df.iterrows():
        # создание кафедры если она не существует в бд
        if row["kathedra"]:
            kathedra_obj = await crud_katedra.get_kathedra_id_by_name(str(row["kathedra"]))
            if kathedra_obj is None:
                kathedra_obj = await crud_katedra.create_kathedra_class(Kathedra(name=str(row["kathedra"])))
        else:
            kathedra_obj = await crud_katedra.get_kathedra_id_by_name('732')
            if kathedra_obj is None:
                kathedra_obj = await crud_katedra.create_kathedra_class(Kathedra(name=732))

        # создание преподавтелей если они не существует в бд
        teacher = await crud_teacher.get_teacher_by_name(row["name"])
        if await crud_teacher.get_teacher_by_name(row["name"]) is None:
            new_teacher = Teacher(name=row["name"], gitlab=row["gitlab"], gitwork=row["gitwork"],
                                  login=str(row["login"]), hashed_passwd=str(row["password"]),
                                  kathedra_id=kathedra_obj.kathedra_id, telegram=row["telegram"])
            await crud_teacher.create_teacher_class(new_teacher)
        else:
            logger.info(f"teacher {teacher.name} already in database")
    return templates.TemplateResponse("./pages/admin/add/add_excel.html", {"request": request})


@admin_router.post("/add_course_group_excel")
async def add_course_group_excel(request: Request, file_course: UploadFile = File(...),
                                 database: Session = Depends(get_db)):
    logger.info("Add course group")
    content = await file_course.read()
    excel_file = io.BytesIO(content)
    df = pd.read_excel(excel_file)
    df = df.applymap(lambda x: str(x) if not pd.isna(x) else None)  # Заполнение NaN значением None
    crud_course = CourseCRUD(database)
    crud_course_group = CourseGroupCRUD(database)
    crud_group = GroupCRUD(database)
    for _, row in df.iterrows():
        course_obj = await crud_course.get_course_by_name(row["name"])
        if course_obj is None:
            new_course = Course(name=row["name"], version=float(row.get("version", 0)), avg_score=0,
                                color=row["color"], fullname=row["fullname"])
            course_obj = await crud_course.create_course_class(new_course)
        groups = await crud_group.get_all_groups()
        for group in groups:
            if group.name.split('-')[-1] == row["group"]:
                new_course_group = CourseGroup(group_id=group.group_id, course_id=course_obj.course_id,
                                               course_over=0, avg_score=0, is_end=False, is_start=False,
                                               avg_score_last_test=0)
                course_group = await crud_course_group.create_course_group_class(new_course_group)

    return templates.TemplateResponse("./pages/admin/add/add_excel.html", {"request": request})


@admin_router.post("/add_teacher_course_excel")
async def add_teacher_course_excel(request: Request, file_teacher_courses: UploadFile = File(...),
                                   database: Session = Depends(get_db)):
    logger.info("Add course group")
    content = await file_teacher_courses.read()
    excel_file = io.BytesIO(content)
    df = pd.read_excel(excel_file)
    df = df.applymap(lambda x: str(x) if not pd.isna(x) else None)  # Заполнение NaN значением None
    crud_course = CourseCRUD(database)
    crud_teacher_course = TeacherCourseCRUD(database)
    crud_teacher = TeacherCRUD(database)
    crud_role = RoleCRUD(database)
    for _, row in df.iterrows():
        # logger.info(row)
        teacher = await crud_teacher.get_teacher_by_name(row["teacher_name"])
        course = await crud_course.get_course_by_name((row["course_name"]))
        role = await crud_role.get_role_by_name(row["role"])
        if role is None:
            role = await crud_role.create_role_class(Role(name=row["role"]))
        teacher_course = TeacherCourse(teacher_id=teacher.teacher_id,
                                       course_id=course.course_id,
                                       role_id=role.role_id)
        await crud_teacher_course.create_teacher_course_class(teacher_course)
    return templates.TemplateResponse("./pages/admin/add/add_excel.html", {"request": request})


async def post_check_res_request(filename: str, content_str: str):
    async with httpx.AsyncClient() as client:
        url = "http://test_skillrate_backend:8000/parse_upload_file"
        payload = filename, content_str
        response = await client.post(url, json=payload)
        return response.text


@admin_router.post("/parse_upload_file")
async def parse_upload_file(request: Request, file_tests: UploadFile = File(...)):
    filename = file_tests.filename.replace(".txt", '')
    content = await file_tests.read()
    content_str = content.decode()
    await post_check_res_request(filename, content_str)
    return templates.TemplateResponse("./pages/admin/add/add_excel.html", {"request": request})
