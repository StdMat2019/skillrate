import asyncio

from sqlalchemy.ext.asyncio import async_sessionmaker
from sqlalchemy.ext.asyncio import create_async_engine

from backend.app.database.models import Base


DATABASE_URL = "postgresql+asyncpg://test_user:test_password@test_db:5432/postgres"

engine = create_async_engine(DATABASE_URL)
Session = async_sessionmaker(engine, expire_on_commit=False)


def pytest_sessionstart(session):
    engine = create_async_engine(DATABASE_URL)
    asyncio.run(create_tables(engine))


def pytest_sessionfinish(session, exitstatus):
    engine = create_async_engine(DATABASE_URL)
    asyncio.run(drop_tables(engine))


async def create_tables(engine):
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


async def drop_tables(engine):
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
