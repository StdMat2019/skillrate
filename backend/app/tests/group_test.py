from datetime import datetime

import pytest

from backend.app.crud.group_crud import GroupCRUD
from backend.app.crud.speciality_crud import SpecialityCRUD
from conftest import Session


@pytest.fixture
async def create_speciality():
    speciality_data = {"number": 56, "name": "Test Speciality"}
    async with Session() as session:
        speciality_crud = SpecialityCRUD(session)
        new_speciality = await speciality_crud.create_speciality(speciality_data)
        yield new_speciality.speciality_id
        await speciality_crud.delete_speciality(new_speciality.speciality_id)


@pytest.mark.asyncio(scope="session")
class TestGroupCRUD:
    async def test_create_and_get_group_by_id(self, create_speciality):
        async for speciality_id in create_speciality:
            group_data = {
                "name": "Group A",
                "start_date": datetime.strptime("2023-01-01", "%Y-%m-%d").date(),
                "end_date": datetime.strptime("2023-12-31", "%Y-%m-%d").date(),
                "speciality_id": speciality_id,
                "count_members": 10
            }
            async with Session() as session:
                crud = GroupCRUD(session)
                group = await crud.create_group(group_data)
                assert group is not None
                del_group = await crud.delete_group(group.group_id)
                assert del_group == group.group_id

    async def test_create_group_with_long_name(self, create_speciality):
        async for speciality_id in create_speciality:
            group_data = {
                "name": "Group E Group E Group E Group E Group E Group E Group E Group E",
                "start_date": datetime.strptime("2023-01-01", "%Y-%m-%d").date(),
                "end_date": datetime.strptime("2023-12-31", "%Y-%m-%d").date(),
                "speciality_id": speciality_id,
                "count_members": 10
            }
            async with Session() as session:
                crud = GroupCRUD(session)
                with pytest.raises(ValueError) as excinfo:
                    await crud.create_group(group_data)
            assert "name is too long" in str(excinfo.value).lower()

    async def test_update_group(self, create_speciality):
        async for speciality_id in create_speciality:
            group_data = {
                "name": "Group E",
                "start_date": datetime.strptime("2023-01-01", "%Y-%m-%d").date(),
                "end_date": datetime.strptime("2023-12-31", "%Y-%m-%d").date(),
                "speciality_id": speciality_id,
                "count_members": 10
            }
            new_name = "Group E Updated"
            async with Session() as session:
                crud = GroupCRUD(session)
                group = await crud.create_group(group_data)
                updated_group = await crud.update_group(group.group_id, {"name": new_name})

                assert group is not None
                assert updated_group.name == new_name
                del_group = await crud.delete_group(group.group_id)
                assert del_group is not None

    async def test_delete_group(self, create_speciality):
        async for speciality_id in create_speciality:
            group_data = {
                "name": "Group E",
                "start_date": datetime.strptime("2023-01-01", "%Y-%m-%d").date(),
                "end_date": datetime.strptime("2023-12-31", "%Y-%m-%d").date(),
                "speciality_id": speciality_id,
                "count_members": 10
            }
            async with Session() as session:
                crud = GroupCRUD(session)
                group = await crud.create_group(group_data)
                await crud.delete_group(group.group_id)
                deleted_group = await crud.get_group_by_id(group.group_id)
            assert group is not None
            assert deleted_group is None
