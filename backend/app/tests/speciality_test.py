import pytest

from backend.app.crud.speciality_crud import SpecialityCRUD
from conftest import Session


@pytest.mark.asyncio(scope="session")
class TestSpecialityCRUD:
    async def test_create_speciality(self):
        speciality_data = {"number": 101, "name": "Engineering"}
        async with Session() as session:
            crud = SpecialityCRUD(session)
            speciality = await crud.create_speciality(speciality_data)
            assert speciality is not None
            assert speciality.name == speciality_data["name"]

    async def test_create_and_get_speciality_by_id(self):
        speciality_data = {"number": 102, "name": "Science"}
        async with Session() as session:
            crud = SpecialityCRUD(session)
            created_speciality = await crud.create_speciality(speciality_data)
            assert created_speciality is not None
            retrieved_speciality = await crud.get_speciality_by_id(created_speciality.speciality_id)
            assert retrieved_speciality is not None
            assert retrieved_speciality.speciality_id == created_speciality.speciality_id

    async def test_create_speciality_with_long_name(self):
        async with Session() as session:
            crud = SpecialityCRUD(session)
            long_name = "a" * 21
            with pytest.raises(ValueError) as excinfo:
                await crud.create_speciality({"number": 103, "name": long_name})
            assert "name is too long" in str(excinfo.value).lower()

    async def test_update_speciality(self):
        speciality_data = {"number": 104, "name": "Mathematics"}
        new_name = "Advanced Mathematics"
        async with Session() as session:
            crud = SpecialityCRUD(session)
            speciality = await crud.create_speciality(speciality_data)
            assert speciality is not None

            updated_speciality = await crud.update_speciality(speciality.speciality_id, {"name": new_name})
            assert updated_speciality.name == new_name

    async def test_delete_speciality(self):
        speciality_data = {"number": 105, "name": "Physics"}
        async with Session() as session:
            crud = SpecialityCRUD(session)
            speciality = await crud.create_speciality(speciality_data)
            assert speciality is not None

            await crud.delete_speciality(speciality.speciality_id)
            deleted_speciality = await crud.get_speciality_by_id(speciality.speciality_id)
            assert deleted_speciality is None
