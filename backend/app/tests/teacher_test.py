import pytest

from backend.app.crud.teacher_crud import TeacherCRUD
from conftest import Session
from fixture import teacher_data


@pytest.mark.asyncio(scope="session")
class TestTeacherCRUD:
    async def test_create_teacher(self, teacher_data):
        async with Session() as session:
            crud = TeacherCRUD(session)
            teacher = await crud.create_teacher(teacher_data)
            assert teacher is not None

    async def test_get_teacher_by_id(self, teacher_data):
        async with Session() as session:
            crud = TeacherCRUD(session)
            teacher = await crud.create_teacher(teacher_data)
            retrieved_teacher = await crud.get_teacher_by_id(teacher.teacher_id)
            assert retrieved_teacher is not None
            assert retrieved_teacher.teacher_id == teacher.teacher_id

    async def test_update_teacher(self, teacher_data):
        async with Session() as session:
            crud = TeacherCRUD(session)
            teacher = await crud.create_teacher(teacher_data)
            update_data = {"name": "Updated Name"}
            updated_teacher = await crud.update_teacher(teacher.teacher_id, update_data)
            assert updated_teacher.name == "Updated Name"

    async def test_delete_teacher(self, teacher_data):
        async with Session() as session:
            crud = TeacherCRUD(session)
            teacher = await crud.create_teacher(teacher_data)
            await crud.delete_teacher(teacher.teacher_id)
            deleted_teacher = await crud.get_teacher_by_id(teacher.teacher_id)
            assert deleted_teacher is None

    async def test_get_all_teachers(self, teacher_data):
        async with Session() as session:
            crud = TeacherCRUD(session)
            await crud.create_teacher(teacher_data)
            teachers = await crud.get_all_teachers()
            assert len(teachers) > 0
