import pytest

from backend.app.crud.student_crud import StudentCRUD
from conftest import Session
from fixture import student_data


@pytest.mark.asyncio(scope="session")
class TestStudentCRUD:
    async def test_create_student(self, student_data):
        async with Session() as session:
            crud = StudentCRUD(session)
            student = await crud.create_student(student_data)
            assert student is not None

    async def test_create_and_get_student_by_id(self, student_data):
        async with Session() as session:
            crud = StudentCRUD(session)
            created_student = await crud.create_student(student_data)
            assert created_student is not None
            assert created_student.name == student_data["name"]
            retrieved_student = await crud.get_student_by_id(created_student.student_id)
            assert retrieved_student is not None
            assert retrieved_student.student_id == created_student.student_id

    async def test_create_student_with_long_name(self):
        async with Session() as session:
            crud = StudentCRUD(session)
            long_name = "a" * 21
            with pytest.raises(ValueError) as excinfo:
                await crud.create_student({"name": long_name, "login": "testlogin1", "hashed_passwd": "testpass"})
            assert "name is too long" in str(excinfo.value).lower()

    async def test_get_student_by_id_with_invalid_id(self):
        async with Session() as session:
            crud = StudentCRUD(session)
            with pytest.raises(ValueError) as excinfo:
                await crud.get_student_by_id("not-a-uuid")
            assert "invalid student_id type" in str(excinfo.value).lower()

    async def test_update_student_with_invalid_id(self):
        async with Session() as session:
            crud = StudentCRUD(session)
            with pytest.raises(ValueError) as excinfo:
                await crud.update_student("not-a-uuid", {"name": "Updated Name"})
            assert "invalid student_id type" in str(excinfo.value).lower()

    async def test_create_and_delete_student(self, student_data):
        async with Session() as session:
            crud = StudentCRUD(session)
            created_student = await crud.create_student(student_data)
            assert created_student is not None
            await crud.delete_student(created_student.student_id)
            deleted_student = await crud.get_student_by_id(created_student.student_id)
            assert deleted_student is None

    async def test_delete_student_with_invalid_id(self):
        async with Session() as session:
            crud = StudentCRUD(session)
            with pytest.raises(ValueError) as excinfo:
                await crud.delete_student("not-a-uuid")
            assert "invalid student_id type" in str(excinfo.value).lower()
