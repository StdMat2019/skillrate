import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from backend.app.crud.course_group_crud import CourseGroupCRUD
# from backend.app.tests.fixture import course_group_data
from conftest import Session


# @pytest.mark.asyncio(scope="session")
# class TestCourseGroupCRUD:
#
#     async def test_create_course_group(self, course_group_data):
#         async with Session() as session:
#             crud = CourseGroupCRUD(session)
#             async for course_group_data_dict in course_group_data:
#                 course_group = await crud.create_course_group(course_group_data_dict)
#                 assert course_group is not None
#                 await crud.delete_course_group(course_group.course_id, course_group.group_id)
