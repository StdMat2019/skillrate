import pytest

from backend.app.crud.skill_crud import SkillCRUD
from conftest import Session


@pytest.fixture
async def create_skill():
    skill_data = {"name": "Test Skill"}
    async with Session() as session:
        skill_crud = SkillCRUD(session)
        new_skill = await skill_crud.create_skill(skill_data)
        yield new_skill.skill_id
        await skill_crud.delete_skill(new_skill.skill_id)


@pytest.mark.asyncio(scope="session")
class TestSkillCRUD:
    async def test_create_and_get_skill_by_id(self, create_skill):
        async for skill_id in create_skill:
            skill_data = {"name": "Skill A"}
            async with Session() as session:
                crud = SkillCRUD(session)
                skill = await crud.create_skill(skill_data)
                assert skill is not None
                del_skill = await crud.delete_skill(skill.skill_id)
                assert del_skill == skill.skill_id

    async def test_create_skill_with_long_name(self, create_skill):
        async for skill_id in create_skill:
            skill_data = {
                "name": "Skill Long Name Skill Long Name Skill Long Name"
            }
            async with Session() as session:
                crud = SkillCRUD(session)
                with pytest.raises(ValueError) as excinfo:
                    await crud.create_skill(skill_data)
            assert "name is too long" in str(excinfo.value).lower()

    async def test_update_skill(self, create_skill):
        async for skill_id in create_skill:
            skill_data = {"name": "Skill B"}
            new_name = "Skill B Updated"
            async with Session() as session:
                crud = SkillCRUD(session)
                skill = await crud.create_skill(skill_data)
                updated_skill = await crud.update_skill(skill.skill_id, {"name": new_name})

                assert skill is not None
                assert updated_skill.name == new_name
                del_skill = await crud.delete_skill(skill.skill_id)
                assert del_skill is not None

    async def test_delete_skill(self, create_skill):
        async for skill_id in create_skill:
            skill_data = {"name": "Skill C"}
            async with Session() as session:
                crud = SkillCRUD(session)
                skill = await crud.create_skill(skill_data)
                await crud.delete_skill(skill.skill_id)
                deleted_skill = await crud.get_skill_by_id(skill.skill_id)
            assert skill is not None
            assert deleted_skill is None
