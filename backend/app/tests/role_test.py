import pytest

from backend.app.crud.role_crud import RoleCRUD
from conftest import Session


@pytest.fixture
async def create_role():
    role_data = {"name": "Test Role"}
    async with Session() as session:
        role_crud = RoleCRUD(session)
        new_role = await role_crud.create_role(role_data)
        yield new_role.role_id
        await role_crud.delete_role(new_role.role_id)


@pytest.mark.asyncio(scope="session")
class TestRoleCRUD:
    async def test_create_and_get_role_by_id(self, create_role):
        async for role_id in create_role:
            role_data = {"name": "Role A"}
            async with Session() as session:
                crud = RoleCRUD(session)
                role = await crud.create_role(role_data)
                assert role is not None
                del_role = await crud.delete_role(role.role_id)
                assert del_role == role.role_id

    async def test_create_role_with_long_name(self, create_role):
        async for role_id in create_role:
            role_data = {
                "name": "Role Long Name Role Long Name Role Long Name"
            }
            async with Session() as session:
                crud = RoleCRUD(session)
                with pytest.raises(ValueError) as excinfo:
                    await crud.create_role(role_data)
            assert "name is too long" in str(excinfo.value).lower()

    async def test_update_role(self, create_role):
        async for role_id in create_role:
            role_data = {"name": "Role B"}
            new_name = "Role B Updated"
            async with Session() as session:
                crud = RoleCRUD(session)
                role = await crud.create_role(role_data)
                updated_role = await crud.update_role(role.role_id, {"name": new_name})

                assert role is not None
                assert updated_role.name == new_name
                del_role = await crud.delete_role(role.role_id)
                assert del_role is not None

    async def test_delete_role(self, create_role):
        async for role_id in create_role:
            role_data = {"name": "Role C"}
            async with Session() as session:
                crud = RoleCRUD(session)
                role = await crud.create_role(role_data)
                await crud.delete_role(role.role_id)
                deleted_role = await crud.get_role_by_id(role.role_id)
            assert role is not None
            assert deleted_role is None
