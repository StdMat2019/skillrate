from datetime import date
from uuid import uuid4

import pytest

from backend.app.crud.student_crud import StudentCRUD
from conftest import Session


@pytest.fixture
def student_data():
    return {
        "name": "Test Student",
        "login": "testlogin",
        "hashed_passwd": "testpassword"
    }


@pytest.fixture
def teacher_data():
    return {
        "name": "John Doe",
        "login": "johndoe",
        "hashed_passwd": "hashedpassword123",
        "gitlab": "johndoe_gitlab"
    }


@pytest.fixture
async def create_test_stat_data():
    async with Session() as session:
        crud = StudentCRUD(session)
        student_data = {
            "name": "Test Student",
            "login": "testlogin",
            "hashed_passwd": "testpassword"
        }
        student = await crud.create_student(student_data)
        return {
            "test_id": uuid4(),
            "student_id": student.student_id,
            "date": date.today(),
            "total_time": 15,
            "total_points": 100,
            "successful_points": 80
        }
