import pytest

from backend.app.crud.course_crud import CourseCRUD
from conftest import Session


@pytest.fixture
async def create_course():
    course_data = {"name": "Test Course", "version": 1, "avg_score": 80, "color": "112345",
                   "fullname": "Full Test Course"}
    async with Session() as session:
        course_crud = CourseCRUD(session)
        new_course = await course_crud.create_course(course_data)
        yield new_course.course_id
        await course_crud.delete_course(new_course.course_id)


@pytest.mark.asyncio(scope="session")
class TestCourseCRUD:
    async def test_create_and_get_course_by_id(self, create_course):
        async for course_id in create_course:
            course_data = {
                "name": "Course A",
                "version": 1,
                "avg_score": 85,
                "color": "FFFFFF",
                "fullname": "Course A Full"
            }
            async with Session() as session:
                crud = CourseCRUD(session)
                course = await crud.create_course(course_data)
                assert course is not None
                del_course = await crud.delete_course(course.course_id)
                assert del_course == course.course_id

    async def test_create_course_with_long_name(self, create_course):
        async for course_id in create_course:
            course_data = {
                "name": "Long Course Name Long Course Name Long Course Name",
                "version": 1,
                "avg_score": 90,
                "color": "F67890",
                "fullname": "Long Course Full Name"
            }
            async with Session() as session:
                crud = CourseCRUD(session)
                with pytest.raises(ValueError) as excinfo:
                    await crud.create_course(course_data)
            assert "name is too long" in str(excinfo.value).lower()

    async def test_update_course(self, create_course):
        async for course_id in create_course:
            course_data = {
                "name": "Course B",
                "version": 1,
                "avg_score": 75,
                "color": "111111",
                "fullname": "Course B Full"
            }
            new_name = "Course B Updated"
            async with Session() as session:
                crud = CourseCRUD(session)
                course = await crud.create_course(course_data)
                updated_course = await crud.update_course(course.course_id, {"name": new_name})

                assert course is not None
                assert updated_course.name == new_name
                del_course = await crud.delete_course(course.course_id)
                assert del_course is not None

    async def test_delete_course(self, create_course):
        async for course_id in create_course:
            course_data = {
                "name": "Course C",
                "version": 1,
                "avg_score": 80,
                "color": "212222",
                "fullname": "Course C Full"
            }
            async with Session() as session:
                crud = CourseCRUD(session)
                course = await crud.create_course(course_data)
                await crud.delete_course(course.course_id)
                deleted_course = await crud.get_course_by_id(course.course_id)
            assert course is not None
            assert deleted_course is None
