import pytest

from backend.app.crud.kathedra_crud import KathedraCRUD
from conftest import Session


@pytest.fixture
async def create_kathedra():
    kathedra_data = {"name": "Test Kathedra"}
    async with Session() as session:
        kathedra_crud = KathedraCRUD(session)
        new_kathedra = await kathedra_crud.create_kathedra(kathedra_data)
        yield new_kathedra.kathedra_id
        await kathedra_crud.delete_kathedra(new_kathedra.kathedra_id)


@pytest.mark.asyncio(scope="session")
class TestKathedraCRUD:
    async def test_create_and_get_kathedra_by_id(self, create_kathedra):
        async for kathedra_id in create_kathedra:
            kathedra_data = {"name": "Kathedra A"}
            async with Session() as session:
                crud = KathedraCRUD(session)
                kathedra = await crud.create_kathedra(kathedra_data)
                assert kathedra is not None
                del_kathedra = await crud.delete_kathedra(kathedra.kathedra_id)
                assert del_kathedra == kathedra.kathedra_id

    async def test_create_kathedra_with_long_name(self, create_kathedra):
        async for kathedra_id in create_kathedra:
            kathedra_data = {
                "name": "Kathedra Long Name Kathedra Long Name Kathedra Long Name"
            }
            async with Session() as session:
                crud = KathedraCRUD(session)
                with pytest.raises(ValueError) as excinfo:
                    await crud.create_kathedra(kathedra_data)
            assert "name is too long" in str(excinfo.value).lower()

    async def test_update_kathedra(self, create_kathedra):
        async for kathedra_id in create_kathedra:
            kathedra_data = {"name": "Kathedra B"}
            new_name = "Kathedra B Updated"
            async with Session() as session:
                crud = KathedraCRUD(session)
                kathedra = await crud.create_kathedra(kathedra_data)
                updated_kathedra = await crud.update_kathedra(kathedra.kathedra_id, {"name": new_name})

                assert kathedra is not None
                assert updated_kathedra.name == new_name
                del_kathedra = await crud.delete_kathedra(kathedra.kathedra_id)
                assert del_kathedra is not None

    async def test_delete_kathedra(self, create_kathedra):
        async for kathedra_id in create_kathedra:
            kathedra_data = {"name": "Kathedra C"}
            async with Session() as session:
                crud = KathedraCRUD(session)
                kathedra = await crud.create_kathedra(kathedra_data)
                await crud.delete_kathedra(kathedra.kathedra_id)
                deleted_kathedra = await crud.get_kathedra_by_id(kathedra.kathedra_id)
            assert kathedra is not None
            assert deleted_kathedra is None
