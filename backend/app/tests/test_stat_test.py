import pytest

from backend.app.crud.test_stat_crud import TestStatCRUD
from fixture import create_test_stat_data
from conftest import Session


@pytest.mark.asyncio(scope="session")
class TestStatTestCRUD:
    async def test_create_test_stat(self, create_test_stat_data):
        async with Session() as session:
            crud = TestStatCRUD(session)
            test_stat_dict = await create_test_stat_data
            test_stat = await crud.create_test_stat(test_stat_dict)
            assert test_stat is not None

    async def test_get_test_stat(self, create_test_stat_data):
        async with Session() as session:
            crud = TestStatCRUD(session)
            test_stat = await crud.create_test_stat(await create_test_stat_data)
            fetched_test_stat = await crud.get_test_stat_by_id(test_stat.test_id)
            assert fetched_test_stat is not None
            assert fetched_test_stat.test_id == test_stat.test_id

    async def test_update_test_stat(self, create_test_stat_data):
        new_total_points = 120
        async with Session() as session:
            crud = TestStatCRUD(session)
            test_stat = await crud.create_test_stat(await create_test_stat_data)
            updated_test_stat = await crud.update_test_stat(test_stat.test_id, {"total_points": new_total_points})
            assert updated_test_stat.total_points == new_total_points

    async def test_delete_test_stat(self, create_test_stat_data):
        async with Session() as session:
            crud = TestStatCRUD(session)
            test_stat = await crud.create_test_stat(await create_test_stat_data)
            await crud.delete_test_stat(test_stat.test_id)
            deleted_test_stat = await crud.get_test_stat_by_id(test_stat.test_id)
            assert deleted_test_stat is None
