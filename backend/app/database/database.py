import os

from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker

from backend.app.database.models import Base


def get_database_url():
    db_host = os.getenv("POSTGRES_HOST")
    db_port = os.getenv("POSTGRES_PORT")
    db_name = os.getenv("POSTGRES_NAME")
    db_user = os.getenv("POSTGRES_USER")
    db_pass = os.getenv("POSTGRES_PASSWORD")
    return f"postgresql+asyncpg://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}"


ENGINE = create_async_engine(get_database_url())
ASYNC_SESSIONLOCAL = sessionmaker(bind=ENGINE, class_=AsyncSession, expire_on_commit=False)


async def init_db():
    """Init database, create all models as tables
    """
    async with ENGINE.begin() as connection:
        await connection.run_sync(Base.metadata.create_all)


async def get_db():
    """Create session/connection for each request
    """
    database = ASYNC_SESSIONLOCAL()
    try:
        yield database
    finally:
        await database.close()
