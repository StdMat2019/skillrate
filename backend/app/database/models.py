import uuid

from sqlalchemy import Column, Integer, String, Date, ForeignKey, Float, Boolean
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy_utils import UUIDType


class Base(DeclarativeBase):
    pass


class Student(Base):
    __tablename__ = 'student'
    student_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    role_id = Column(UUIDType, ForeignKey('role.role_id'), nullable=False)
    name = Column(String(30), nullable=False)
    name_rp = Column(String(30), nullable=True)
    fullname = Column(String(30), nullable=False)
    login = Column(String(30), nullable=False)
    hashed_passwd = Column(String(300), nullable=False)
    gitlab = Column(String(30), nullable=True)
    gitwork = Column(String(30), nullable=True)
    telegram = Column(String(30), nullable=True)
    years_over = Column(Integer, nullable=True, default=0)
    completed_courses = Column(Integer, nullable=True, default=0)
    total_score = Column(Float, nullable=True, default=0)
    notice = Column(String(200), nullable=True)
    group_id = Column(UUIDType, ForeignKey('group.group_id'), nullable=True)

    def get_dict(self):
        student_dict = {
            'student_id': str(self.student_id),
            'role_id': str(self.role_id),
            'name': self.name,
            'name_rp': self.name_rp,
            'gitlab': self.gitlab,
            'login': self.login,
            'hashed_passwd': self.hashed_passwd,
            'telegram': self.telegram,
            'years_over': self.years_over,
            'completed_courses': self.completed_courses,
            'total_score': self.total_score,
            'gitwork': self.gitwork,
            'notice': self.notice,
            'group_id': str(self.group_id) if self.group_id else None
        }
        return student_dict


class Teacher(Base):
    __tablename__ = 'teacher'
    teacher_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    name = Column(String(30), nullable=False)
    gitlab = Column(String(30), nullable=True)
    gitwork = Column(String(30), nullable=True)
    login = Column(String(30), nullable=False)
    hashed_passwd = Column(String(100), nullable=False)
    kathedra_id = Column(UUIDType, ForeignKey('kathedra.kathedra_id'), nullable=True)
    telegram = Column(String(30), nullable=True)

    def get_dict(self):
        teacher_dict = {
            'teacher_id': str(self.teacher_id),
            'name': self.name,
            'gitlab': self.gitlab,
            'gitwork': self.gitwork,
            'login': self.login,
            'kathedra_id': str(self.kathedra_id) if self.kathedra_id else None,
            'hashed_passwd': self.hashed_passwd,
            'telegram': self.telegram
        }
        return teacher_dict


class Speciality(Base):
    __tablename__ = 'speciality'
    speciality_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    number = Column(Integer, nullable=True)
    name = Column(String(10), nullable=False)
    fullname = Column(String(40), nullable=True)

    def get_dict(self):
        return {"speciality_id": self.speciality_id, "number": self.number,
                "name": self.name, "fullname": self.fullname}


class Group(Base):
    __tablename__ = 'group'
    group_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    name = Column(String(50), nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=False)
    speciality_id = Column(UUIDType, ForeignKey('speciality.speciality_id'), nullable=False)
    count_members = Column(Integer, default=0)

    def get_dict(self):
        return {
            'group_id': str(self.group_id),
            'name': self.name,
            'start_date': str(self.start_date),
            'end_date': str(self.end_date),
            'speciality_id': str(self.speciality_id),
            'count_members': self.count_members
        }


class Course(Base):
    __tablename__ = 'course'
    course_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    name = Column(String(20))
    version = Column(Float)
    avg_score = Column(Float)
    color = Column(String(7))
    fullname = Column(String(50))

    def get_dict(self):
        course_dict = {
            'course_id': str(self.course_id),
            'name': self.name,
            'version': self.version,
            'avg_score': self.avg_score,
            'color': self.color,
            'fullname': self.fullname
        }
        return course_dict


class Kathedra(Base):
    __tablename__ = 'kathedra'
    kathedra_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    name = Column(String(10))
    fullname = Column(String(50), nullable=True)

    def get_dict(self):
        return {"kathedra_id": self.kathedra_id,
                "name": self.name, "fullname": self.fullname}


class Role(Base):
    __tablename__ = 'role'
    role_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    name = Column(String(30))


class Skill(Base):
    __tablename__ = 'skill'
    skill_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    name = Column(String(40))

    def get_dict(self):
        test_stat_dict = {
            'skill_id': str(self.skill_id),
            'name': self.name
        }
        return test_stat_dict

    def get_dict_name_skill_id(self):
        test_stat_dict = {
            self.name: str(self.skill_id)
        }
        return test_stat_dict

    def get_dict_skill_id_name(self):
        test_stat_dict = {
            str(self.skill_id): self.name
        }
        return test_stat_dict


class TestStat(Base):
    __tablename__ = 'test_stat'
    test_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    student_id = Column(UUIDType, ForeignKey('student.student_id'))
    course_id = Column(UUIDType, ForeignKey('course.course_id'))
    collection_name = Column(String(50))
    date = Column(Date)
    total_time = Column(String)
    total_points = Column(Float)
    successful_points = Column(Float)
    test_mark = Column(Float)

    def get_dict(self):
        test_stat_dict = {
            'test_id': str(self.test_id),
            'student_id': str(self.student_id),
            'course_id': str(self.course_id),
            'collection_name': str(self.collection_name),
            'date': self.date,
            'total_time': self.total_time,
            'total_points': self.total_points,
            'successful_points': self.successful_points,
            'test_mark': self.test_mark
        }
        return test_stat_dict


class CourseGroup(Base):
    __tablename__ = 'course_group'
    group_id = Column(UUIDType, ForeignKey('group.group_id'), primary_key=True)
    course_id = Column(UUIDType, ForeignKey('course.course_id'), primary_key=True)
    course_over = Column(Integer)
    avg_score = Column(Float)
    is_end = Column(Boolean)
    is_start = Column(Boolean)
    avg_score_last_test = Column(Float)


class TestGroupCourse(Base):
    __tablename__ = 'test_group_course'
    test_group_course_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    course_id = Column(UUIDType, ForeignKey('course.course_id'))
    collection_name = Column(String(50))
    test_fullname = Column(String(60))
    is_open = Column(Boolean)

    def get_dict(self):
        return {
            "test_group_course_id": str(self.test_group_course_id),
            "course_id": str(self.course_id),
            "collection_name": self.collection_name,
            "test_fullname": self.test_fullname,
            "is_open": self.is_open
        }


class TeacherCourse(Base):
    __tablename__ = 'teacher_course'
    teacher_id = Column(UUIDType, ForeignKey('teacher.teacher_id'), primary_key=True)
    course_id = Column(UUIDType, ForeignKey('course.course_id'), primary_key=True)
    role_id = Column(UUIDType, ForeignKey('role.role_id'), nullable=False)


class StudentCourse(Base):
    __tablename__ = 'student_course'
    result = Column(Float)
    course_id = Column(UUIDType, ForeignKey('course.course_id'), primary_key=True)
    student_id = Column(UUIDType, ForeignKey('student.student_id'), primary_key=True)
    complete_percent = Column(Float)
    avg_score = Column(Float)

    def get_dict(self):
        student_course_dict = {
            'result': self.result,
            'course_id': str(self.course_id),
            'student_id': str(self.student_id),
            'complete_percent': self.complete_percent,
            'avg_score': self.avg_score
        }
        return student_course_dict


class StudentSkill(Base):
    __tablename__ = 'student_skill'
    student_id = Column(UUIDType, ForeignKey('student.student_id'), primary_key=True)
    total_points = Column(Float)
    successful_points = Column(Float)
    skill_id = Column(UUIDType, ForeignKey('skill.skill_id'), primary_key=True)
    group_id = Column(UUIDType, ForeignKey('group.group_id'))

    def get_dict(self):
        student_skill_dict = {
            'student_id': str(self.student_id),
            'total_points': self.total_points,
            'successful_points': self.successful_points,
            'skill_id': str(self.skill_id),
            'group_id': str(self.group_id)
        }
        return student_skill_dict


class CourseSkill(Base):
    __tablename__ = 'course_skill'
    course_id = Column(UUIDType, ForeignKey('course.course_id'), primary_key=True)
    skill_id = Column(UUIDType, ForeignKey('skill.skill_id'), primary_key=True)
    learn_require = Column(UUIDType, ForeignKey('skill.skill_id'), nullable=True)


class CourseSpeciality(Base):
    __tablename__ = 'course_speciality'
    course_id = Column(UUIDType, ForeignKey('course.course_id'), primary_key=True)
    speciality_id = Column(UUIDType, ForeignKey('speciality.speciality_id'), primary_key=True)


class QuestStat(Base):
    __tablename__ = 'quest_stat'
    quest_stat_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    test_id = Column(UUIDType, ForeignKey('test_stat.test_id'))
    student_id = Column(UUIDType, ForeignKey('student.student_id'))
    question_id = Column(UUIDType, nullable=False)
    total_points = Column(Float)
    successful_points = Column(Float)
    old_result = Column(Boolean, default=False)
    # old_result значение нужно устанавливать в True когда задается вопрос заново
    # это поле позволит фильтровать неправильные вопросы которые задаются несколько раз

    def get_dict(self):
        return {
            "quest_stat_id": str(self.quest_stat_id),
            "test_id": str(self.test_id),
            "student_id": str(self.student_id),
            "question_id": str(self.question_id),
            "total_points": self.total_points,
            "successful_points": self.successful_points,
            "old_result": self.old_result

        }

    def get_dict_shot(self):
        return {
            str(self.question_id): [self.total_points, self.successful_points, self.old_result]
        }
