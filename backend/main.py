import uvicorn
from fastapi import Depends
from fastapi import FastAPI
from fastapi import Form
from fastapi import HTTPException
from fastapi import Request
from fastapi import Response
from fastapi import status
from fastapi.responses import RedirectResponse
from fastapi.routing import APIRouter
from loguru import logger
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session


from .app.api.admin_handler import admin_router
from .app.api.teacher_handler import teacher_router
from .app.api.user_handler import user_router
from .app.database.database import init_db
from .app.database.database import get_db
from .app.crud.student_crud import StudentCRUD
from .app.crud.teacher_crud import TeacherCRUD
from .app.auth.hash_passwd import verify
from .app.auth.auth import create_access_token
# pylint: disable=invalid-name

templates = Jinja2Templates(directory="frontend/src/pages")

app = FastAPI(title="SkillRate")

main_api_router = APIRouter()

# set routes to the app instance
main_api_router.include_router(user_router, prefix="/student", tags=["student"])
main_api_router.include_router(teacher_router, prefix="/teacher", tags=["teacher"])
main_api_router.include_router(admin_router, prefix="/admin", tags=["admin"])
app.include_router(main_api_router)

# Добавим свойство Base.metadata.create_all в зависимости от инициализации базы данных
app.state.db_initialized = False


@app.on_event("startup")
async def startup_event():
    await init_db()
    app.state.db_initialized = True


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8001)


@app.get("/")
async def home(request: Request):
    return RedirectResponse(url=app.url_path_for("login"), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/login")
async def login(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})


@app.post('/login')
async def get_token(request: Request, login: str = Form(...), password: str = Form(...),
                    database: Session = Depends(get_db)):
    # logger.info(f"{login, password}")
    try:
        crud_stud = StudentCRUD(database)
        crud_teach = TeacherCRUD(database)

        stud = await crud_stud.get_student(login)
        teach = await crud_teach.get_teacher(login)

        if stud:
            response = RedirectResponse(url=app.url_path_for("student_stat"), status_code=status.HTTP_303_SEE_OTHER)
            access_token = create_access_token(data={'login': login})
            if not verify(stud.hashed_passwd, password):
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Wrong password')
        elif teach:
            response = RedirectResponse(url=app.url_path_for("teacher_sub"), status_code=status.HTTP_303_SEE_OTHER)
            access_token = create_access_token(data={'login': login})
            if not verify(teach.hashed_passwd, password):
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Wrong password')
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Invalid credentials')

    except HTTPException as ex:
        return templates.TemplateResponse("index.html",
                                          {"request": request, "alert_msg": "Incorrect password or login"},
                                          status_code=400)

    # logger.info(f"{login}")
    response.set_cookie(key="access_token", value=access_token)
    return response


@app.get("/logout")
async def logout(request: Request, response: Response):
    # Удаление всех куков, связанных с аутентификацией
    response.delete_cookie("access_token")
    return templates.TemplateResponse("index.html", {"request": request})
