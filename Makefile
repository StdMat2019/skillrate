DOCKER_COMPOSE_FILE=docker-compose.yml

all: build run

run:
	docker compose -f $(DOCKER_COMPOSE_FILE) up -d

stop:
	docker compose -f $(DOCKER_COMPOSE_FILE) down

clean: stop
	docker compose -f $(DOCKER_COMPOSE_FILE) rm -f
	docker compose -f $(DOCKER_COMPOSE_FILE) down --volumes --remove-orphans
	docker image prune -f

build:
	docker compose -f $(DOCKER_COMPOSE_FILE) build