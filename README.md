# SkillRate

Веб-приложение оценивания студентов с учётом состава тем дисциплин.
Основной проект API для взаимодействия с Postgre и реализующий пользовательский веб интерфейс

[фигма](https://www.figma.com/file/Ft5sZ5MfDSfHqif1MamZXS/SkillRate?type=design&node-id=249-2775&mode=design&t=3rZghXe68igQkxXv-0)

## Быстрый старт

Запуск БД Postgre и приложения SkillRate

```bash
make
```

Остановка БД Postgre и приложения SkillRate

```bash
make stop
```

Построение Docker образов для сервисов

```bash
docker compose -f docker-compose.yml build
```

Запуск приложения через Docker compose

```bash
docker compose -f docker-compose.yml up
```

Построение Docker образов для сервисов

```bash
docker compose -f docker-compose-tests.yml build
```

Запуск тестов через Docker compose

```bash
docker compose -f docker-compose-tests.yml up
```

**backend (FastAPI и PostgreSQL):**

- `main.py` - Основной файл FastAPI приложения
- `app/` - Директория с модулями FastAPI приложения
    - `api/` - Директория с файлами, отвечающими за API маршруты(роуты)
    - `auth/` - Директория с файлами, отвечающими за авторизацию
    - `crud/` - Директория с функциями для выполнения операций CRUD в БД
    - `database/` - Директория с моделями и скриптами для работы с базой данных
        - `models.py` - Файл описывающий модели для БД
        - `database.py` - Файл описывающий модели для БД
    - `tests/` - Директория с тестами

**frontend:**

- `src/` - Исходный код фронтенда
    - `components/` - Компоненты, используемые в приложении
    - `pages/` - Страницы приложения
        - `admin/` - Страницы приложения админа
        - `student/` - Страницы приложения студента
        - `teacher/` - Страницы приложения преподавателя
        - `index.html` - Точка входа в приложение

**excel_templates:** Шаблоны excel файлов для заполнения БД

**docker:**

- `Dockerfile` - Файл для создания Docker-образа приложения
- `Dockerfile-tests` - Файл для создания Docker-образа тестов
- `docker-compose.yml` - Файл для настройки и запуска контейнеров приложения
- `docker-compose-tests.yml` - Файл для настройки и запуска контейнеров тестов
- `.env` - Файл с конфигурационными переменными: секретные ключи, настройки базы данных
- `requirements.txt` - Файл с зависимостями Python

**Возможные ошибки**

При запуске compose порт 5432 занят. Необходимо удалить процесс, который занял порт.
Получить PID процесса можно при помощи команды:

```bash
lsof -i :5432
```

